package dst.ass1.nosql.impl;

import com.mongodb.*;
import com.mongodb.util.JSON;

import dst.ass1.jpa.model.ITask;
import dst.ass1.jpa.util.Constants;
import dst.ass1.nosql.IMongoDbDataLoader;
import dst.ass1.nosql.MongoTestData;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class MongoDbDataLoader implements IMongoDbDataLoader {

    private EntityManager em;

    public MongoDbDataLoader(EntityManager em) {
        this.em = em;
    }

    @Override
    public void loadData() throws Exception {
        Mongo mongo = null;
        MongoTestData testData = new MongoTestData();
        List<ITask> finishedTasks = getFinishedTasks();

        try {
            System.out.println("Init MongoDB...");
            mongo = new Mongo();
            DB db = mongo.getDB("dst");
            DBCollection collection = db.getCollection(Constants.COLL_TASKRESULT);

            System.out.println(String.format("Inserting %d finished tasks...", finishedTasks.size()));
            for (ITask task : finishedTasks) {
                Object data = JSON.parse(testData.getStringData(task.getId().intValue()));
                String dataDesc = testData.getDataDesc(task.getId().intValue());

                DBObject taskObject = new BasicDBObject("task_id", task.getId())
                        .append("last_updated", task.getTaskProcessing().getEnd().getTime())
                        .append(dataDesc, data);

                collection.insert(taskObject);
            }

            System.out.println("Creating task_id index...");
            collection.ensureIndex("task_id");

            System.out.println("loadData() done.");
        }
        finally
        {
            if (mongo != null)
                mongo.close();
        }
    }

    @SuppressWarnings("unchecked")
    private List<ITask> getFinishedTasks() {
        System.out.println("Loading finished tasks...");
        Query query = em.createNamedQuery(Constants.Q_ALLFINISHEDTASKS);
        return (List<ITask>) query.getResultList();
    }
}
