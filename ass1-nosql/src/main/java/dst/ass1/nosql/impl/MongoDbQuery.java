package dst.ass1.nosql.impl;

import com.mongodb.*;
import dst.ass1.jpa.util.Constants;
import dst.ass1.nosql.IMongoDbQuery;

import java.util.ArrayList;
import java.util.List;

public class MongoDbQuery implements IMongoDbQuery {

    private DB db;

    public MongoDbQuery(DB db) {
        this.db = db;
    }

    private DBCollection getDBCollection() {
        System.out.println(String.format("Getting MongoDB collection %s...", Constants.COLL_TASKRESULT));
        return db.getCollection(Constants.COLL_TASKRESULT);
    }

    @Override
    public Long findLastUpdatedByTaskId(Long id) {

        DBCollection collection = getDBCollection();

        System.out.println(String.format("Querying for task_id %d...", id));
        DBObject query = new BasicDBObject("task_id", id);
        DBObject task = collection.findOne(query);

        if (task == null) {
            System.out.println(String.format("Task ID %d not found!", id));
            throw new RuntimeException("Task ID not found");
        }

        System.out.println(String.format("Found: %s", task.toString()));

        Object updated = task.get("last_updated");
        return Long.parseLong(updated.toString());
    }

    @Override
    public List<Long> findLastUpdatedGt(Long time) {

        DBCollection collection = getDBCollection();
        DBCursor cursor = null;
        List<Long> task_ids = new ArrayList<Long>();

        try {
            System.out.println(String.format("Querying for tasks with last_updated > %d...", time));

            // $gt operator: http://docs.mongodb.org/manual/reference/operator/query/gt/
            DBObject query = new BasicDBObject("last_updated", new BasicDBObject("$gt", time));

            // field projection: http://docs.mongodb.org/manual/tutorial/project-fields-from-query-results/
            DBObject fields = new BasicDBObject("task_id", 1);

            cursor = collection.find(query, fields);
            while (cursor.hasNext()) {
                DBObject task = cursor.next();
                System.out.println(String.format("Found: %s", task.toString()));

                Object task_id = task.get("task_id");
                task_ids.add(Long.parseLong(task_id.toString()));
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }

        System.out.println(String.format("Found %d matching tasks.", task_ids.size()));
        return task_ids;
    }

    @Override
    public List<DBObject> mapReduceWorkflow() {

        DBCollection collection = getDBCollection();

        // project every document into a list of its interesting properties
        String map =
                "function() {" +
                "   delete this._id;" +
                "   delete this.task_id;" +
                "   delete this.last_updated; " +
                "   for(var prop in this) emit(prop, 1);" +
                "}";

        // sum up count of every property
        String reduce = "function(prop, value) { return Array.sum(value); }";

        System.out.println("Building inline map-reduce command...");
        MapReduceCommand mapReduceCommand = new MapReduceCommand(
                collection, map, reduce, null, MapReduceCommand.OutputType.INLINE, null);
        MapReduceOutput mapReduceOutput = collection.mapReduce(mapReduceCommand);

        List<DBObject> results = new ArrayList<DBObject>();
        for (DBObject object : mapReduceOutput.results()) {
            System.out.println("Result: " + object.toString());
            results.add(object);
        }

        System.out.println(String.format("Found %d different properties.", results.size()));
        return results;
    }
}
