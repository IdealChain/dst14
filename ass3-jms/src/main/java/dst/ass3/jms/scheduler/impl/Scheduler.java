package dst.ass3.jms.scheduler.impl;

import dst.ass3.dto.InfoTaskWrapperDTO;
import dst.ass3.dto.NewTaskWrapperDTO;
import dst.ass3.dto.TaskWrapperDTO;
import dst.ass3.jms.BaseClient;
import dst.ass3.jms.scheduler.IScheduler;
import dst.ass3.jms.scheduler.IScheduler.ISchedulerListener.InfoType;
import org.apache.openejb.api.LocalClient;

import javax.annotation.Resource;
import javax.jms.*;

@SuppressWarnings("UnusedDeclaration")
@LocalClient
public class Scheduler extends BaseClient implements IScheduler, MessageListener {

    private MessageProducer serverProducer;
    private MessageConsumer schedulerConsumer;
    private ISchedulerListener schedulerListener;

    @Resource(name = "dst.queue.Server")
    private Queue serverQueue;
    @Resource(name = "dst.queue.Scheduler")
    private Queue schedulerQueue;

    @Override
    public void start() {
        try {
            super.start(this);
            System.out.println("Scheduler: start()");

            // create producer and consumer
            serverProducer = session.createProducer(serverQueue);
            schedulerConsumer = session.createConsumer(schedulerQueue);

            // set me as MessageListener
            schedulerConsumer.setMessageListener(this);

            // start JMS messaging connection
            connection.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        System.out.println("Scheduler: stop()");
        try {

            schedulerConsumer.close();
            serverProducer.close();

            super.stop();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void assign(long taskId) {
        System.out.println(String.format("Scheduler: assign(%d)", taskId));
        NewTaskWrapperDTO dto = new NewTaskWrapperDTO(taskId);

        try {
            Message message = session.createObjectMessage(dto);
            serverProducer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void info(long taskWrapperId) {
        System.out.println(String.format("Scheduler: info(%d)", taskWrapperId));
        InfoTaskWrapperDTO dto = new InfoTaskWrapperDTO(taskWrapperId);

        try {
            Message message = session.createObjectMessage(dto);
            serverProducer.send(message);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setSchedulerListener(ISchedulerListener listener) {
        schedulerListener = listener;
    }

    @Override
    public void onMessage(Message message) {
        System.out.println("Scheduler: onMessage()");

        try {
            InfoType infoType = InfoType.valueOf(message.getStringProperty("infotype"));
            TaskWrapperDTO taskWrapper = (TaskWrapperDTO) ((ObjectMessage) message).getObject();

            if (schedulerListener != null)
                schedulerListener.notify(infoType, taskWrapper);

        } catch (JMSException e) {
            e.printStackTrace();
        }


    }
}
