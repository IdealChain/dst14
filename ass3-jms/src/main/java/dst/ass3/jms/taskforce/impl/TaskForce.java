package dst.ass3.jms.taskforce.impl;

import dst.ass3.dto.RateTaskWrapperDTO;
import dst.ass3.dto.TaskWrapperDTO;
import dst.ass3.jms.BaseClient;
import dst.ass3.jms.taskforce.ITaskForce;
import dst.ass3.jms.taskforce.ITaskForce.ITaskForceListener.TaskWrapperDecideResponse;
import dst.ass3.model.LifecycleState;
import org.apache.openejb.api.LocalClient;

import javax.annotation.Resource;
import javax.jms.*;

@SuppressWarnings("UnusedDeclaration")
@LocalClient
public class TaskForce extends BaseClient implements ITaskForce, MessageListener {

    private String name;

    private MessageProducer serverProducer;
    private MessageConsumer taskForceConsumer;
    private ITaskForceListener taskForceListener;

    @Resource(name = "dst.queue.TaskForce")
    private Queue taskForceQueue;
    @Resource(name = "dst.queue.Server")
    private Queue serverQueue;

    public TaskForce(String name) {
        this.name = name;
    }

    @Override
    public void start() {
        try {
            super.start(this, name);
            System.out.println(String.format("TaskForce %s: start()", name));

            // create producer and consumer
            serverProducer = session.createProducer(serverQueue);
            taskForceConsumer = session.createConsumer(taskForceQueue);

            // set me as MessageListener
            taskForceConsumer.setMessageListener(this);

            // start JMS messaging connection
            connection.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        System.out.println(String.format("TaskForce %s: stop()", name));
        try {

            taskForceConsumer.close();
            serverProducer.close();

            super.stop();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setTaskForceListener(ITaskForceListener listener) {
        taskForceListener = listener;
    }

    @Override
    public void onMessage(Message message) {
        System.out.println(String.format("TaskForce %s: onMessage()", name));

        try {
            TaskWrapperDTO taskWrapper = (TaskWrapperDTO) ((ObjectMessage) message).getObject();
            RateTaskWrapperDTO rateTaskWrapper = new RateTaskWrapperDTO(taskWrapper);
            rateTaskWrapper.setRatedBy(name);

            if (taskForceListener == null) {
                System.out.println(String.format("TaskForce %s: no TaskForceListener!", name));
                return;
            }

            TaskWrapperDecideResponse decision = taskForceListener.decideTask(rateTaskWrapper, name);

            // interpret decision and react accordingly
            if (decision.resp == ITaskForceListener.TaskWrapperResponse.ACCEPT) {
                rateTaskWrapper.setComplexity(decision.complexity);
                rateTaskWrapper.setState(LifecycleState.READY_FOR_PROCESSING);

            } else if (decision.resp == ITaskForceListener.TaskWrapperResponse.DENY) {
                rateTaskWrapper.setState(LifecycleState.PROCESSING_NOT_POSSIBLE);

            } else {
                throw new RuntimeException("Illegal TaskWrapperResponse");
            }

            serverProducer.send(session.createObjectMessage(rateTaskWrapper));

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
