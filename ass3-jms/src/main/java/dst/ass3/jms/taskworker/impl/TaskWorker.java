package dst.ass3.jms.taskworker.impl;

import dst.ass3.dto.ProcessTaskWrapperDTO;
import dst.ass3.jms.BaseClient;
import dst.ass3.jms.taskworker.ITaskWorker;
import dst.ass3.model.Complexity;
import dst.ass3.model.LifecycleState;
import org.apache.openejb.api.LocalClient;

import javax.annotation.Resource;
import javax.jms.*;

@SuppressWarnings("UnusedDeclaration")
@LocalClient
public class TaskWorker extends BaseClient implements ITaskWorker, MessageListener {

    private String name;
    private String taskForce;
    private Complexity complexity;

    private MessageProducer serverProducer;
    private MessageConsumer taskWorkerSubscriber;
    private ITaskWorkerListener taskWorkerListener;

    @Resource(name = "dst.topic.TaskWorker")
    private Topic taskWorkerTopic;
    @Resource(name = "dst.queue.Server")
    private Queue serverQueue;

    public TaskWorker(String name, String taskForce, Complexity complexity) {
        this.name = name;
        this.taskForce = taskForce;
        this.complexity = complexity;
    }

    @Override
    public void start() {
        try {
            super.start(this, name);
            System.out.println(String.format("TaskWorker %s: start()", name));

            String messageSelector = String.format(
                    "taskforce = '%s' AND complexity = '%s'", taskForce, complexity);

            // create producer and durable topic subscriber
            serverProducer = session.createProducer(serverQueue);
            taskWorkerSubscriber = session.createDurableSubscriber(
                    taskWorkerTopic, name, messageSelector, false);

            // set me as MessageListener
            taskWorkerSubscriber.setMessageListener(this);

            // start JMS messaging connection
            connection.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        System.out.println(String.format("TaskWorker %s: stop()", name));
        try {

            taskWorkerSubscriber.close();
            serverProducer.close();

            super.stop();
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setTaskWorkerListener(ITaskWorkerListener listener) {
        this.taskWorkerListener = listener;
    }

    @Override
    public void onMessage(Message message) {
        System.out.println(String.format("TaskWorker %s: onMessage()", name));

        try {
            ProcessTaskWrapperDTO processTaskWrapper = (ProcessTaskWrapperDTO) ((ObjectMessage) message).getObject();

            if (taskWorkerListener == null) {
                System.out.println(String.format("TaskWorker %s: no TaskWorkerListener!", name));
                return;
            }

            taskWorkerListener.waitTillProcessed(
                    processTaskWrapper,
                    name,
                    processTaskWrapper.getComplexity(),
                    processTaskWrapper.getRatedBy());

            processTaskWrapper.setState(LifecycleState.PROCESSED);
            serverProducer.send(session.createObjectMessage(processTaskWrapper));

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
