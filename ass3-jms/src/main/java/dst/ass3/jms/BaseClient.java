package dst.ass3.jms;

import org.apache.openejb.api.LocalClient;

import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;
import java.util.UUID;

@LocalClient
public abstract class BaseClient {

    protected Session session;
    protected Connection connection;

    @SuppressWarnings("UnusedDeclaration")
    @Resource(name = "dst.Factory")
    private ConnectionFactory connectionFactory;

    public void start(Object client) throws NamingException, JMSException {
        String clientID = UUID.randomUUID().toString();
        start(client, clientID);
    }

    public void start(Object client, String clientID) throws NamingException, JMSException {
        System.out.println(String.format("BaseClient: start(%s)", clientID));

        // obtain a JNDI connection (as seen in AbstractJMSTest)
        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.client.LocalInitialContextFactory");
        InitialContext ctx = new InitialContext(p);
        ctx.bind("inject", client);

        // create JMS connection
        connection = connectionFactory.createConnection();

        // unique client ID necessary for durable subscriptions
        connection.setClientID(clientID);

        // JMS session
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    public void stop() throws JMSException {
        System.out.println(String.format("BaseClient: stop(%s)", connection.getClientID()));

        session.close();
        connection.close();
    }
}
