package dst.ass3.jms;

import dst.ass3.dto.*;
import dst.ass3.jms.scheduler.IScheduler.ISchedulerListener.InfoType;
import dst.ass3.model.Complexity;
import dst.ass3.model.LifecycleState;
import dst.ass3.model.TaskWrapper;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Core JMS relaying server.
 *
 * Forwards incoming messages to the intended clients and keeps the database
 * information up to date.
 */
@SuppressWarnings("UnusedDeclaration")
@MessageDriven(mappedName = "dst.queue.Server")
public class Server implements MessageListener {

    @PersistenceContext(unitName = "dst")
    private EntityManager em;

    @Resource
    private MessageDrivenContext mdctx;

    @Resource(name = "dst.Factory")
    private ConnectionFactory connectionFactory;
    @Resource(name = "dst.queue.Scheduler")
    private Queue schedulerQueue;
    @Resource(name = "dst.queue.TaskForce")
    private Queue taskForceQueue;
    @Resource(name = "dst.topic.TaskWorker")
    private Topic taskWorkerTopic;

    @Override
    public void onMessage(Message message) {
        try {

            Object dto = ((ObjectMessage) message).getObject();
            System.out.println(String.format(
                    "Server: onMessage(%s: %s)", dto.getClass().getName(), dto));

            if (dto instanceof NewTaskWrapperDTO) // from Scheduler: create new task
                createTask((NewTaskWrapperDTO) dto);

            else if (dto instanceof InfoTaskWrapperDTO) // from Scheduler: get info on task
                taskInfo((InfoTaskWrapperDTO) dto);

            else if (dto instanceof RateTaskWrapperDTO) // from TaskForces: task rated for processing
                taskRated((RateTaskWrapperDTO) dto);

            else if (dto instanceof ProcessTaskWrapperDTO) // from TaskWorkers: task processed
                taskProcessed((ProcessTaskWrapperDTO) dto);

            else
                throw new RuntimeException("Unknown message object type: " + dto.getClass().getName());

        } catch (JMSException e) {
            e.printStackTrace();
            mdctx.setRollbackOnly();
        }
    }

    private void createTask(NewTaskWrapperDTO dto) throws JMSException {

        // create new task(wrapper)
        TaskWrapper taskWrapper = new TaskWrapper();
        taskWrapper.setTaskId(dto.getTaskId());
        taskWrapper.setState(LifecycleState.ASSIGNED);
        taskWrapper.setComplexity(Complexity.UNRATED);

        em.persist(taskWrapper);

        // send confirmation to Scheduler
        sendToScheduler(new TaskWrapperDTO(taskWrapper), InfoType.CREATED);

        // forward task to next available task-force
        sendToTaskForce(new TaskWrapperDTO(taskWrapper));
    }

    private void taskInfo(InfoTaskWrapperDTO dto) throws JMSException {
        TaskWrapper taskWrapper = em.find(TaskWrapper.class, dto.getTaskWrapperId());
        if (taskWrapper == null)
            return;

        // send task info to scheduler
        sendToScheduler(new TaskWrapperDTO(taskWrapper), InfoType.INFO);
    }

    private void taskRated(RateTaskWrapperDTO dto) throws JMSException {
        TaskWrapper taskWrapper = em.find(TaskWrapper.class, dto.getId());
        if (taskWrapper == null)
            return;
        taskWrapper.setState(dto.getState());
        taskWrapper.setRatedBy(dto.getRatedBy());

        if (dto.getState() == LifecycleState.READY_FOR_PROCESSING) {
            // update complexity, pass task to the task-workers
            taskWrapper.setComplexity(dto.getComplexity());
            sendToTaskWorkers(new ProcessTaskWrapperDTO(taskWrapper));

        } else if (dto.getState() == LifecycleState.PROCESSING_NOT_POSSIBLE) {
            // inform scheduler about denied task
            sendToScheduler(new TaskWrapperDTO(taskWrapper), InfoType.DENIED);
        }
    }

    private void taskProcessed(ProcessTaskWrapperDTO dto) throws JMSException {
        TaskWrapper taskWrapper = em.find(TaskWrapper.class, dto.getId());
        if (taskWrapper == null || dto.getState() == taskWrapper.getState())
            return;

        // update state, inform scheduler
        taskWrapper.setState(dto.getState());
        sendToScheduler(new TaskWrapperDTO(taskWrapper), InfoType.PROCESSED);

    }

    private void sendToScheduler(TaskWrapperDTO dto, InfoType info) throws JMSException {
        System.out.println(String.format("Server: sendToScheduler(%s, %s)", dto, info.name()));

        // create JMS connection, session, and producer
        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer schedulerProducer = session.createProducer(schedulerQueue);

        // create message
        Message message = session.createObjectMessage(dto);
        message.setStringProperty("infotype", info.name());

        connection.start();
        schedulerProducer.send(message);
        System.out.println("Sent message to scheduler.");

        schedulerProducer.close();
        session.close();
        connection.close();
    }

    private void sendToTaskForce(TaskWrapperDTO dto) throws JMSException {
        System.out.println(String.format("Server: sendToTaskForce(%s)", dto));

        // create JMS connection, session, and producer
        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer taskForceProducer = session.createProducer(taskForceQueue);

        // create message
        Message message = session.createObjectMessage(dto);

        connection.start();
        taskForceProducer.send(message);
        System.out.println("Sent message to task forces.");

        taskForceProducer.close();
        session.close();
        connection.close();
    }

    private void sendToTaskWorkers(ProcessTaskWrapperDTO dto) throws JMSException {
        System.out.println(String.format("Server: sendToTaskWorkers(%s)", dto));

        // create JMS connection, session, and producer
        Connection connection = connectionFactory.createConnection();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageProducer taskWorkerProducer = session.createProducer(taskWorkerTopic);

        // create message
        Message message = session.createObjectMessage(dto);
        message.setStringProperty("taskforce", dto.getRatedBy());
        message.setStringProperty("complexity", dto.getComplexity().name());

        connection.start();
        taskWorkerProducer.send(message);
        System.out.println("Sent message to task workers.");

        taskWorkerProducer.close();
        session.close();
        connection.close();
    }
}
