package dst.ass2.ejb.interceptor;

import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.impl.AuditLog;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@SuppressWarnings("UnusedDeclaration")
public class AuditInterceptor {

    @PersistenceContext(unitName = "dst")
    private EntityManager em;

    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception {

        IAuditLog log = new AuditLog(context);
        System.out.println(String.format("AuditInterceptor: '%s' starting", log.getMethod()));

        try {
            // proceed to method call (or next interceptor)
            Object result = context.proceed();

            if (result != null)
                log.setResult(result.toString());

            System.out.println(String.format(
                    "AuditInterceptor: '%s' finished => '%s'", log.getMethod(), log.getResult()));

            return result;

        } catch (Exception e) {

            // method threw exception
            log.setResult(e.toString());
            System.out.println(String.format(
                    "AuditInterceptor: '%s' exceptioned (%s)", log.getMethod(), e.toString()));

            // re-throw intercepted exception for handling
            throw e;

        } finally {
            em.persist(log);
        }
    }
}
