package dst.ass2.ejb.ws;

import dst.ass2.ejb.ws.impl.GetStatsRequest;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * This interface defines the getters and setters of the
 * GetStatsRequest Web service request object.
 */
@XmlJavaTypeAdapter(GetStatsRequest.Adapter.class)
public interface IGetStatsRequest {

    /**
     * @return maximum number of processings in the statistics
     */
    int getMaxProcessings();

    /**
     * @param maxProcessings maximum number of processings in the statistics
     */
    void setMaxProcessings(int maxProcessings);

}
