package dst.ass2.ejb.ws.impl;

import dst.ass2.ejb.ws.IGetStatsRequest;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class GetStatsRequest implements IGetStatsRequest {

    @XmlElement(required = true)
    private int maxProcessings = 0;

    @Override
    public int getMaxProcessings() {
        return maxProcessings;
    }

    @Override
    public void setMaxProcessings(int maxProcessings) {
        this.maxProcessings = maxProcessings;
    }

    public static class Adapter extends XmlAdapter<GetStatsRequest, IGetStatsRequest> {

        @Override
        public IGetStatsRequest unmarshal(GetStatsRequest v) throws Exception {
            return v;
        }

        @Override
        public GetStatsRequest marshal(IGetStatsRequest v) throws Exception {
            return (GetStatsRequest) v;
        }
    }
}
