package dst.ass2.ejb.ws.impl;

import dst.ass2.ejb.dto.StatisticsDTO;
import dst.ass2.ejb.ws.IGetStatsResponse;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class GetStatsResponse implements IGetStatsResponse {

    @XmlElement(required = true)
    private StatisticsDTO statistics;

    private GetStatsResponse() {
    }

    public GetStatsResponse(StatisticsDTO statistics) {
        this.statistics = statistics;
    }

    @Override
    public StatisticsDTO getStatistics() {
        return statistics;
    }

    public static class Adapter extends XmlAdapter<GetStatsResponse, IGetStatsResponse> {

        @Override
        public GetStatsResponse unmarshal(GetStatsResponse v) throws Exception {
            return v;
        }

        @Override
        public GetStatsResponse marshal(IGetStatsResponse v) throws Exception {
            return (GetStatsResponse) v;
        }
    }
}
