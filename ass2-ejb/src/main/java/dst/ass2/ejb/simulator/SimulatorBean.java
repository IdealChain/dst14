package dst.ass2.ejb.simulator;

import dst.ass1.jpa.model.ITaskProcessing;
import dst.ass1.jpa.model.TaskStatus;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@SuppressWarnings("UnusedDeclaration")
@Stateless
public class SimulatorBean {

    @PersistenceContext(unitName = "dst")
    private EntityManager em;

    @Schedule(second = "*/5", minute = "*", hour = "*", info = "Task Processing Simulator")
    public void simulate() {
        List<ITaskProcessing> runningTPs = em
                .createNamedQuery("TaskProcessing.runningTasks", ITaskProcessing.class)
                .setParameter("now", new Date())
                .getResultList();

        if (runningTPs.isEmpty())
            return;

        System.out.println(String.format("SimulatorBean: finishing %d task(s).", runningTPs.size()));

        for (ITaskProcessing runningTP : runningTPs) {
            runningTP.setStatus(TaskStatus.FINISHED);
            runningTP.setEnd(new Date());
        }
    }

}