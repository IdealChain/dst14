package dst.ass2.ejb.management;

import dst.ass2.ejb.management.interfaces.IPriceManagementBean;
import dst.ass2.ejb.model.IPrice;
import dst.ass2.ejb.model.impl.Price;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.*;

@Startup
@Singleton
@Local(IPriceManagementBean.class)
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class PriceManagementBean implements IPriceManagementBean {

    private NavigableMap<Integer, IPrice> prices;

    @PersistenceContext(unitName = "dst")
    private EntityManager em;

    @PostConstruct
    private void loadPrices() {
        System.out.println("PriceManagementBean: loading prices...");
        TypedQuery<Price> query = em.createNamedQuery("Price.allPriceSteps", Price.class);

        prices = new TreeMap<Integer, IPrice>();
        for (Price price : query.getResultList())
            prices.put(price.getNrOfHistoricalTasks(), price);

        System.out.println(String.format("%d prices loaded.", prices.size()));
    }

    @Override
    @Lock(LockType.READ)
    public BigDecimal getPrice(Integer nrOfHistoricalTasks) {
        System.out.println(String.format("PriceManagementBean: getPrice(%d)", nrOfHistoricalTasks));

        Map.Entry<Integer, IPrice> entry = prices.floorEntry(nrOfHistoricalTasks);
        if (entry == null)
            entry = prices.firstEntry();
        if (entry == null)
            return BigDecimal.ZERO;

        return entry.getValue().getPrice();
    }

    @Override
    @Lock(LockType.WRITE)
    public void setPrice(Integer nrOfHistoricalTasks, BigDecimal price) {
        System.out.println(String.format(
                "PriceManagementBean: setPrice(%d, %s)", nrOfHistoricalTasks, price));

        if(prices.containsKey(nrOfHistoricalTasks)) {
            IPrice p = prices.get(nrOfHistoricalTasks);

            if (!p.getPrice().equals(price)) {
                System.out.println(String.format(
                        "Updating existing price step %d: %s => %s",
                        nrOfHistoricalTasks, p.getPrice(), price));

                p = em.merge(p);
                p.setPrice(price);
                prices.put(nrOfHistoricalTasks, p);
            }

            return;
        }

        Price p = new Price();
        p.setPrice(price);
        p.setNrOfHistoricalTasks(nrOfHistoricalTasks);
        em.persist(p);

        prices.put(nrOfHistoricalTasks, p);
    }

    @Override
    @Lock(LockType.WRITE)
    public void clearCache() {
        System.out.println("PriceManagementBean: clear cache.");
        prices.clear();
    }
}
