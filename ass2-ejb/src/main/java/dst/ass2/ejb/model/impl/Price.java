package dst.ass2.ejb.model.impl;

import dst.ass2.ejb.model.IPrice;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@NamedQuery(
        name = "Price.allPriceSteps",
        query = "SELECT p FROM Price p"
)
public class Price implements IPrice, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private Integer nrOfHistoricalTasks = 0;
    private BigDecimal price = new BigDecimal(0);

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getNrOfHistoricalTasks() {
        return nrOfHistoricalTasks;
    }

    @Override
    public void setNrOfHistoricalTasks(Integer nrOfHistoricalTasks) {
        this.nrOfHistoricalTasks = nrOfHistoricalTasks;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
