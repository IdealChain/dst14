package dst.ass2.ejb.model.impl;

import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.model.IAuditParameter;

import javax.interceptor.InvocationContext;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NamedQuery(
        name = "AuditLog.getLogs",
        query = "SELECT al FROM AuditLog al " +
                "ORDER BY al.invocationTime ASC"
)
public class AuditLog implements IAuditLog, Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String method;
    private String result;
    private Date invocationTime;
    @OneToMany(targetEntity = AuditParameter.class, mappedBy = "auditLog", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<IAuditParameter> auditParameters;

    public AuditLog(InvocationContext context) {

        method = context.getMethod().getName();
        invocationTime = new Date();
        auditParameters = new ArrayList<IAuditParameter>();

        Object[] params = context.getParameters();
        if (params != null) {
            for (int i = 0; i < params.length; i++)
                auditParameters.add(new AuditParameter(this, i, params[i]));
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    protected AuditLog() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getMethod() {
        return method;
    }

    @Override
    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String getResult() {
        return result;
    }

    @Override
    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public Date getInvocationTime() {
        return invocationTime;
    }

    @Override
    public void setInvocationTime(Date invocationTime) {
        this.invocationTime = invocationTime;
    }

    @Override
    public List<IAuditParameter> getParameters() {
        if (auditParameters == null)
            auditParameters = new ArrayList<IAuditParameter>();

        return auditParameters;
    }

    @Override
    public void setParameters(List<IAuditParameter> parameters) {
        this.auditParameters = parameters;
    }
}
