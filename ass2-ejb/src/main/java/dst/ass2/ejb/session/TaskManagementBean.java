package dst.ass2.ejb.session;

import dst.ass1.jpa.model.*;
import dst.ass1.jpa.model.impl.*;
import dst.ass2.ejb.dto.AssignmentDTO;
import dst.ass2.ejb.interceptor.AuditInterceptor;
import dst.ass2.ejb.session.exception.AssignmentException;
import dst.ass2.ejb.session.interfaces.ITaskManagementBean;

import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

@Stateful
@Remote(ITaskManagementBean.class)
@Interceptors(AuditInterceptor.class)
public class TaskManagementBean implements ITaskManagementBean {

    @PersistenceContext(unitName = "dst")
    private EntityManager em;

    private IUser user;
    private TaskPlanner planner;

    @SuppressWarnings("UnusedDeclaration")
    @PostConstruct
    private void init() {
        planner = new TaskPlanner(em);
    }

    @Override
    public void addTask(Long platformId, Integer numWorkUnits, String context,
                        List<String> settings) throws AssignmentException {

        System.out.println(String.format(
                "TaskManagementBean: addTask(%d, %d, %s)",
                platformId, numWorkUnits, context));

        if (em.find(WorkPlatform.class, platformId) == null)
            throw new AssignmentException("Invalid platformId");

        AssignmentDTO task = new AssignmentDTO(platformId, numWorkUnits, context, settings, null);
        planner.planTask(task);
    }

    @Override
    public void login(String username, String password)
            throws AssignmentException {

        System.out.println(String.format("TaskManagementBean: login(%s)", username));

        List<User> result = em
                .createNamedQuery("User.verifyUser", User.class)
                .setParameter("name", username)
                .setParameter("pw", MD5(password))
                .getResultList();

        if (result.isEmpty())
            throw new AssignmentException("Invalid credentials");

        System.out.println(String.format("Logged in: %s", username));
        user = result.get(0);
    }

    @Override
    public void removeTasksForPlatform(Long platformId) {
        System.out.println(String.format("TaskManagementBean: removeTasksForPlatform(%d)", platformId));
        planner.removeTasksForPlatform(platformId);
    }

    @Override
    @Remove(retainIfException = true)
    @TransactionAttribute(value = TransactionAttributeType.REQUIRED)
    public void submitAssignments() throws AssignmentException {
        System.out.println("TaskManagementBean: submitAssignments()");

        if (user == null)
            throw new AssignmentException("Login is required for task submission");

        TaskSubmissionLockSingleton.getInstance().lock();
        try {
            Map<Long, ITaskWorker> freeWorkers = new HashMap<Long, ITaskWorker>();
            for (ITaskWorker worker : em
                    .createNamedQuery("TaskWorker.free", ITaskWorker.class)
                    .getResultList()) {
                freeWorkers.put(worker.getId(), worker);
            }

            for (AssignmentDTO planned : planner.getPlannedTasks()) {

                if (!freeWorkers.keySet().containsAll(planned.getWorkerIds()))
                    throw new AssignmentException("Originally planned workers not available");

                ITask task = createTaskEntity(planned, freeWorkers);
                em.persist(task);
            }
        } finally {
            TaskSubmissionLockSingleton.getInstance().unlock();
        }

        planner.clear();
    }

    @Override
    public List<AssignmentDTO> getCache() {
        return planner.getPlannedTasks();
    }

    private ITask createTaskEntity(AssignmentDTO plannedTask, Map<Long, ITaskWorker> workerMap) {

        List<ITaskWorker> workers = new ArrayList<ITaskWorker>();
        for (Long workerId : plannedTask.getWorkerIds()) {
            workers.add(workerMap.get(workerId));
        }

        ITaskProcessing tp = new TaskProcessing();
        tp.setStart(new Date());
        tp.setStatus(TaskStatus.SCHEDULED);
        tp.setTaskWorkers(workers);

        IMetadata meta = new Metadata();
        meta.setContext(plannedTask.getContext());
        meta.setSettings(plannedTask.getSettings());

        ITask task = new Task();
        task.setUser(user);
        task.setTaskProcessing(tp);
        task.setMetadata(meta);

        return task;
    }

    private static byte[] MD5(String password) throws AssignmentException {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            return md.digest(password.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new AssignmentException("No MD5 algorithm", e);
        }
    }
}

class TaskPlanner {

    private EntityManager em;
    private List<AssignmentDTO> plannedTasks = new ArrayList<AssignmentDTO>();

    public TaskPlanner(EntityManager em) {
        this.em = em;
    }

    public void planTask(AssignmentDTO task) throws AssignmentException {

        List<Long> plannedTaskWorkers = new ArrayList<Long>();
        for (AssignmentDTO plannedTask : plannedTasks)
            plannedTaskWorkers.addAll(plannedTask.getWorkerIds());

        List<ITaskWorker> taskWorkers = em
                .createNamedQuery("TaskWorker.freeOnPlatform", ITaskWorker.class)
                .setParameter("platformId", task.getPlatformId())
                .getResultList();

        Integer freeCapacity = 0;
        Integer unitsToPlan = task.getNumWorkUnits();
        List<Long> workerIds = new ArrayList<Long>();

        for (ITaskWorker tw : taskWorkers) {
            if (plannedTaskWorkers.contains(tw.getId()))
                continue;

            freeCapacity += tw.getWorkUnitCapacity();

            if (unitsToPlan > 0) {
                workerIds.add(tw.getId());
                unitsToPlan -= Math.min(unitsToPlan, tw.getWorkUnitCapacity());
            }
        }

        if (unitsToPlan > 0)
            throw new AssignmentException("Not enough free capacity available on this platform");

        task.setWorkerIds(workerIds);
        plannedTasks.add(task);

        System.out.println(String.format("Platform %d:", task.getPlatformId()));
        System.out.println("Planned: " + task.toString());
        System.out.println(String.format(
                "Free worker capacity: %d (-%d) => %d",
                freeCapacity,
                task.getNumWorkUnits(),
                freeCapacity - task.getNumWorkUnits()));
    }

    public void removeTasksForPlatform(Long platformId) {
        Iterator<AssignmentDTO> iter = plannedTasks.iterator();
        while (iter.hasNext()) {
            AssignmentDTO task = iter.next();
            if (task.getPlatformId().equals(platformId))
                iter.remove();
        }
    }

    public List<AssignmentDTO> getPlannedTasks() {
        return Collections.unmodifiableList(plannedTasks);
    }

    public void clear() {
        plannedTasks.clear();
    }
}

/**
 * Simple reentrant lock singleton for securing the critical section (between querying free workers
 * and assigning them their new tasks) from other users / stateful session beans.
 *
 * Should allow to synchronize multiple stateful session beans in a common ClassLoader environment.
 * Tough luck though for clustered or distributed environments...
 */
class TaskSubmissionLockSingleton {
    private static final ReentrantLock LOCK_INSTANCE = new ReentrantLock();

    private TaskSubmissionLockSingleton() {
    }

    public static ReentrantLock getInstance() {
        return LOCK_INSTANCE;
    }
}