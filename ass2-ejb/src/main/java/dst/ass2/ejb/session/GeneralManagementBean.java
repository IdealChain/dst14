package dst.ass2.ejb.session;

import dst.ass1.jpa.model.*;
import dst.ass2.ejb.dto.AuditLogDTO;
import dst.ass2.ejb.dto.BillDTO;
import dst.ass2.ejb.management.interfaces.IPriceManagementBean;
import dst.ass2.ejb.model.IAuditLog;
import dst.ass2.ejb.session.interfaces.IGeneralManagementBean;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

@Stateless
@Remote(IGeneralManagementBean.class)
public class GeneralManagementBean implements IGeneralManagementBean {

    @PersistenceContext(unitName = "dst")
    private EntityManager em;

    @EJB
    private IPriceManagementBean priceManagementBean;

    @Override
    public void addPrice(Integer nrOfHistoricalTasks, BigDecimal price) {
        priceManagementBean.setPrice(nrOfHistoricalTasks, price);
    }

    @Override
    @Asynchronous
    public Future<BillDTO> getBillForUser(String username) throws Exception {
        System.out.println(String.format("GeneralManagementBean.getBillForUser(%s)", username));
        IUser user = getUser(username);

        Integer paidTasksCount = em.createNamedQuery("Task.numPaidForUser", Long.class)
                .setParameter("user", user)
                .getSingleResult()
                .intValue();

        List<ITask> unpaidTasks = em.createNamedQuery("Task.unpaidForUser", ITask.class)
                .setParameter("user", user)
                .getResultList();

        BillDTO bill = new BillDTO();
        bill.setUsername(user.getUsername());

        List<BillDTO.BillPerTask> taskBills = new ArrayList<BillDTO.BillPerTask>();
        BigDecimal totalPrice = BigDecimal.ZERO;

        for (ITask task : unpaidTasks) {

            BillDTO.BillPerTask taskBill = bill.new BillPerTask();
            populateTaskBill(taskBill, task, user, paidTasksCount);
            System.out.println(taskBill);

            taskBills.add(taskBill);
            totalPrice = totalPrice.add(taskBill.getTaskCosts());
            task.setPaid(true);
            paidTasksCount++;
        }

        bill.setBills(taskBills);
        bill.setTotalPrice(totalPrice);
        System.out.println(bill);

        return new AsyncResult<BillDTO>(bill);
    }

    private void populateTaskBill(BillDTO.BillPerTask taskBill, ITask task, IUser user, Integer paidTasksCount) {
        List<ITaskWorker> workers = task.getTaskProcessing().getTaskWorkers();

        if (workers.isEmpty())
            throw new RuntimeException("Finished task not assigned to any workers");

        // by definition: all workers of a task must belong to the same platform
        IWorkPlatform platform = workers.get(0).getTaskForce().getWorkPlatform();
        Double discount = getUserDiscount(user, platform);

        BigDecimal costsSetup = priceManagementBean.getPrice(paidTasksCount);
        BigDecimal costsProcessing = platform.getCostsPerWorkUnit().multiply(
                new BigDecimal(task.getProcessingTime() / 60));
        BigDecimal costsTotal = costsSetup.add(costsProcessing).multiply(new BigDecimal(1 - discount));

        System.out.println(String.format(
                "setupCosts: %d paid tasks: %s",
                paidTasksCount, roundCost(costsSetup)));

        System.out.println(String.format(
                "processingCosts: %s * %ds / 60 = %s",
                platform.getCostsPerWorkUnit(), task.getProcessingTime(), roundCost(costsProcessing)));

        System.out.println(String.format(
                "taskCosts: (%s + %s) * (1 - %.2f) = %s",
                roundCost(costsSetup), roundCost(costsProcessing), discount, roundCost(costsTotal)));

        taskBill.setNumberOfWorkers(workers.size());
        taskBill.setProcessingCosts(roundCost(costsProcessing));
        taskBill.setSetupCosts(roundCost(costsSetup));
        taskBill.setTaskCosts(roundCost(costsTotal));
        taskBill.setTaskId(task.getId());
    }

    private IUser getUser(String username) {
        try {
            return em.createNamedQuery("User.byName", IUser.class)
                    .setParameter("name", username)
                    .getSingleResult();

        } catch (NoResultException e) {
            throw new RuntimeException("User does not exist");
        }
    }

    private Double getUserDiscount(IUser user, IWorkPlatform platform) {
        Double discount = (double) 0;
        List<IMembership> memberships = em
                .createNamedQuery("Membership.forUserAndPlatform", IMembership.class)
                .setParameter("user", user)
                .setParameter("platform", platform)
                .getResultList();

        for (IMembership membership : memberships) {
            if (membership.getDiscount() > discount)
                discount = membership.getDiscount();
        }
        return discount;
    }

    private BigDecimal roundCost(BigDecimal cost) {
        return cost.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public List<AuditLogDTO> getAuditLogs() {
        System.out.println("GeneralManagementBean: getAuditLogs()");

        List<AuditLogDTO> auditLogDTOs = new ArrayList<AuditLogDTO>();
        for (IAuditLog log : em.createNamedQuery("AuditLog.getLogs", IAuditLog.class).getResultList()) {
            auditLogDTOs.add(new AuditLogDTO(log));
            System.out.println(new AuditLogDTO(log).toString());
        }

        return auditLogDTOs;
    }

    @Override
    public void clearPriceCache() {
        priceManagementBean.clearCache();
    }
}
