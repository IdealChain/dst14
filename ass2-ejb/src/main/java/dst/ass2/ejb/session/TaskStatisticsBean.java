package dst.ass2.ejb.session;

import dst.ass1.jpa.model.ITaskProcessing;
import dst.ass1.jpa.model.IWorkPlatform;
import dst.ass2.ejb.dto.StatisticsDTO;
import dst.ass2.ejb.session.exception.WebServiceException;
import dst.ass2.ejb.session.interfaces.ITaskStatisticsBean;
import dst.ass2.ejb.ws.Constants;
import dst.ass2.ejb.ws.IGetStatsRequest;
import dst.ass2.ejb.ws.IGetStatsResponse;
import dst.ass2.ejb.ws.impl.GetStatsResponse;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.soap.Addressing;
import java.util.List;

@Addressing
@Stateless
@Remote(ITaskStatisticsBean.class)
@WebService(
        name = Constants.NAME,
        portName = Constants.PORT_NAME,
        serviceName = Constants.SERVICE_NAME,
        targetNamespace = Constants.NAMESPACE,
        endpointInterface = "dst.ass2.ejb.session.interfaces.ITaskStatisticsBean"
)
public class TaskStatisticsBean implements ITaskStatisticsBean {

    @PersistenceContext(unitName = "dst")
    private EntityManager em;

    @Action(
            input = Constants.NAMESPACE + "/StatisticsForPlatformRequest",
            output = Constants.NAMESPACE + "/StatisticsForPlatformResponse",
            fault = {
                    @FaultAction(
                            className = WebServiceException.class,
                            value = Constants.NAMESPACE + "/StatisticsForPlatformFault")
            }
    )
    @WebMethod(operationName = Constants.OP_GET_STATS)
    public IGetStatsResponse getStatisticsForPlatform(
            IGetStatsRequest request,
            @WebParam(header = true, targetNamespace = Constants.NAMESPACE, partName = "platformName") String platformName)
            throws WebServiceException {

        System.out.println(String.format("TaskStatisticsBean: getStatisticsForPlatform(%s)", platformName));

        if (request == null)
            throw new WebServiceException("A request is required");

        int maxProcessings = request.getMaxProcessings();
        List<IWorkPlatform> platform = em
                .createNamedQuery("WorkPlatform.byName", IWorkPlatform.class)
                .setParameter("name", platformName)
                .getResultList();

        if (platform.isEmpty()) {
            System.out.println("WorkPlatform not found: " + platformName);
            throw new WebServiceException("Invalid work platform name: " + platformName);
        }

        StatisticsDTO stat = getStatistics(platform.get(0), maxProcessings);
        return new GetStatsResponse(stat);
    }

    private StatisticsDTO getStatistics(IWorkPlatform platform, int maxProcessings) {

        List<ITaskProcessing> processings = em
                .createNamedQuery("TaskProcessing.finishedForPlatform", ITaskProcessing.class)
                .setParameter("platform", platform)
                .setParameter("limit", maxProcessings)
                .getResultList();

        StatisticsDTO stat = new StatisticsDTO();
        stat.setName(platform.getName());
        for (ITaskProcessing processing : processings)
            stat.addProcessing(processing);

        return stat;
    }

}
