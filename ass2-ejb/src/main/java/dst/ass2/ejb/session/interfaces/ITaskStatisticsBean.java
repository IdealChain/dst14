package dst.ass2.ejb.session.interfaces;

import dst.ass2.ejb.session.exception.WebServiceException;
import dst.ass2.ejb.ws.Constants;
import dst.ass2.ejb.ws.IGetStatsRequest;
import dst.ass2.ejb.ws.IGetStatsResponse;

import javax.ejb.Remote;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.soap.Addressing;

/**
 * This is the interface of the statistics Web Service.
 */
@Addressing
@Remote
@WebService(
        name = Constants.NAME,
        portName = Constants.PORT_NAME,
        serviceName = Constants.SERVICE_NAME,
        targetNamespace = Constants.NAMESPACE
)
public interface ITaskStatisticsBean {

    /**
     * Get statistics for a given platform.
     *
     * @param request The request object with parameters
     * @param request Name of the platform
     * @return statistics for the platform with the specified name.
     */
    @Action(
            input = Constants.NAMESPACE + "/StatisticsForPlatformRequest",
            output = Constants.NAMESPACE + "/StatisticsForPlatformResponse",
            fault = {
                    @FaultAction(
                            className = WebServiceException.class,
                            value = Constants.NAMESPACE + "/StatisticsForPlatformFault")
            }
    )
    @WebMethod(operationName = Constants.OP_GET_STATS)
    IGetStatsResponse getStatisticsForPlatform(
            IGetStatsRequest request,
            @WebParam(header = true, targetNamespace = Constants.NAMESPACE, partName = "platformName") String platformName)
            throws WebServiceException;

}
