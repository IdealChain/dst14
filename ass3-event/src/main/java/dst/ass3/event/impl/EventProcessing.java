package dst.ass3.event.impl;

import com.espertech.esper.client.*;
import dst.ass3.dto.TaskWrapperDTO;
import dst.ass3.event.Constants;
import dst.ass3.event.IEventProcessing;
import dst.ass3.model.ITaskWrapper;
import dst.ass3.model.LifecycleState;

public class EventProcessing implements IEventProcessing {

    private static final String serviceProviderName = "EsperEngineDST";

    private EPServiceProvider serviceProvider;
    private EPRuntime runtime;

    @Override
    public void initializeAll(StatementAwareUpdateListener listener, boolean debug) {
        System.out.println("EventProcessing: initializeAll()");

        // create esper config (register TaskWrapper class from JMS part)
        Configuration config = new Configuration();
        config.addEventType(Constants.EVENT_TASK, ITaskWrapper.class.getName());

        if (debug) {
            config.getEngineDefaults().getLogging().setEnableExecutionDebug(true);
            config.getEngineDefaults().getLogging().setEnableTimerDebug(false);
            config.getEngineDefaults().getLogging().setEnableQueryPlan(false);
        }

        // obtain service provider and administrator
        serviceProvider = EPServiceProviderManager.getProvider(serviceProviderName, config);
        runtime = serviceProvider.getEPRuntime();
        EPAdministrator admin = serviceProvider.getEPAdministrator();

        defineEventTypes(admin, listener);
        createTriggers(admin, listener);
        createEPLQueries(admin, listener);
    }

    /**
     * Define event types (=schemas) dynamically
     */
    private void defineEventTypes(EPAdministrator admin, StatementAwareUpdateListener listener) {

        admin.createEPL("create schema " + Constants.EVENT_TASK_ASSIGNED +
                " as (taskId long, timestamp long)")
                .addListener(listener);

        admin.createEPL("create schema " + Constants.EVENT_TASK_PROCESSED +
                " as (taskId long, timestamp long)")
                .addListener(listener);

        admin.createEPL("create schema " + Constants.EVENT_TASK_DURATION +
                " as (taskId long, duration long)")
                .addListener(listener);
    }

    /**
     * Three EPL queries which generate events for every type (triggers)
     */
    private void createTriggers(EPAdministrator admin, StatementAwareUpdateListener listener) {

        admin.createEPL("insert into " + Constants.EVENT_TASK_ASSIGNED +
                " select taskId as taskId, current_timestamp() as timestamp " +
                " from TaskWrapper(state.name()='" + LifecycleState.ASSIGNED.name() + "')")
                .addListener(listener);

        admin.createEPL("insert into " + Constants.EVENT_TASK_PROCESSED +
                " select taskId as taskId, current_timestamp() as timestamp " +
                " from TaskWrapper(state.name()='" + LifecycleState.PROCESSED.name() + "')")
                .addListener(listener);

        admin.createEPL("insert into " + Constants.EVENT_TASK_DURATION +
                " select processed.taskId as taskId, processed.timestamp - assigned.timestamp as duration " +
                " from " + Constants.EVENT_TASK_ASSIGNED + ".win:length(10000) as assigned, " +
                Constants.EVENT_TASK_PROCESSED + ".win:length(10000) as processed " +
                " where assigned.taskId = processed.taskId")
                .addListener(listener);
    }

    /**
     * Three EPL queries with listener that outputs to STDOUT
     */
    private void createEPLQueries(EPAdministrator admin, StatementAwareUpdateListener listener) {

        StatementAwareUpdateListener stdoutListener = new StatementAwareUpdateListener() {
            @Override
            public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement statement, EPServiceProvider serviceProvider) {
                for (EventBean event : newEvents) {
                    System.out.println(String.format(
                            "NEW EVENT: %s, UNDERLYING: %s",
                            event.getEventType().getName(),
                            event.getUnderlying()
                    ));
                }
            }
        };

        // taskId of all events of type TaskDuration
        EPStatement statTaskDuration = admin.createEPL("select taskId as taskId " +
                " from " + Constants.EVENT_TASK_DURATION + ".std:lastevent()");
        statTaskDuration.addListener(listener);
        statTaskDuration.addListener(stdoutListener);

        // avg of duration of all TaskDuration events within the last 15 seconds
        EPStatement statAvgDuration = admin.createEPL("select avg(duration) as " + Constants.EVENT_AVG_TASK_DURATION +
                " from " + Constants.EVENT_TASK_DURATION + ".win:time(15)");
        statAvgDuration.addListener(listener);
        statAvgDuration.addListener(stdoutListener);

        // detect task-wrappers which have 3 times attempted and failed to execute (pattern matching)
        EPStatement statProcessingAttempted = admin.createEPL("select p1 from pattern [ every " +
                "    p1=TaskWrapper(state.name()='" + LifecycleState.READY_FOR_PROCESSING.name() + "') " +
                " -> p2=TaskWrapper(state.name()='" + LifecycleState.PROCESSING_NOT_POSSIBLE.name() + "' and p1.taskId = p2.taskId)@consume(1) " +
                " -> p3=TaskWrapper(state.name()='" + LifecycleState.READY_FOR_PROCESSING.name() + "' and p2.taskId = p3.taskId)@consume(2) " +
                " -> p4=TaskWrapper(state.name()='" + LifecycleState.PROCESSING_NOT_POSSIBLE.name() + "' and p3.taskId = p4.taskId)@consume(3) " +
                " -> p5=TaskWrapper(state.name()='" + LifecycleState.READY_FOR_PROCESSING.name() + "' and p4.taskId = p5.taskId)@consume(4) " +
                " -> p6=TaskWrapper(state.name()='" + LifecycleState.PROCESSING_NOT_POSSIBLE.name() + "' and p5.taskId = p6.taskId)@consume(5) " +
                "]");
        statProcessingAttempted.addListener(listener);
        statProcessingAttempted.addListener(stdoutListener);
    }

    @Override
    public void addEvent(ITaskWrapper taskWrapper) {
        System.out.println(String.format("EventProcessing: addEvent(%s)", taskWrapper));
        TaskWrapperDTO dto = new TaskWrapperDTO(taskWrapper);
        runtime.sendEvent(dto);
    }

    @Override
    public void close() {
        System.out.println("EventProcessing: close()");
        if (serviceProvider != null) {
            serviceProvider.destroy();
            serviceProvider = null;
        }
    }
}
