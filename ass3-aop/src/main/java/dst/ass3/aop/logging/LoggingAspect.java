package dst.ass3.aop.logging;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

@Aspect
@SuppressWarnings("UnusedDeclaration")
public class LoggingAspect {

    @Before("execution(void dst.ass3.aop.IPluginExecutable.execute()) && " +
            "!@annotation(dst.ass3.aop.logging.Invisible)")
    public void interceptBefore(JoinPoint joinPoint) {
        JoinPointLogger logger = new JoinPointLogger(joinPoint);
        logger.LogMessage("Plugin %s started to execute", logger.getTarget().getName());
    }

    @After("execution(void dst.ass3.aop.IPluginExecutable.execute()) && " +
            "!@annotation(dst.ass3.aop.logging.Invisible)")
    public void interceptAfter(JoinPoint joinPoint) {
        JoinPointLogger logger = new JoinPointLogger(joinPoint);
        logger.LogMessage("Plugin %s is finished", logger.getTarget().getName());
    }
}

/**
 * Provides logging facilities for plugin executions.
 * If the plugin contains a logger field, that is used, else fallback
 * to STDOUT.
 */
class JoinPointLogger {

    JoinPoint joinPoint;
    Logger pluginLogger;

    public Class getTarget() {
        return joinPoint.getTarget().getClass();
    }

    public JoinPointLogger(JoinPoint joinPoint) {
        this.joinPoint = joinPoint;

        // check if plugin has a Logger field
        try {
            for (Field field : getTarget().getDeclaredFields()) {
                if (field.getType().equals(Logger.class)) {
                    field.setAccessible(true);
                    pluginLogger = (Logger) field.get(joinPoint.getThis());
                    return;
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Logs the given message (String.format syntax).
     */
    public void LogMessage(String format, Object... args) {
        String msg = String.format(format, args);

        if (pluginLogger != null)
            pluginLogger.logp(Level.INFO, LoggingAspect.class.getName(), "execution", msg);
        else
            System.out.println(msg);
    }

}
