package dst.ass3.aop.management;

import dst.ass3.aop.IPluginExecutable;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Keeps a list of currently executing plugins and checks
 * periodically whether they exceeded their allowed timeout duration.
 */
@Aspect
@SuppressWarnings("UnusedDeclaration")
public class ManagementAspect {

    public static final int CheckIntervalMillis = 1000;

    private final Map<IPluginExecutable, Long> pluginTimeouts =
            Collections.synchronizedMap(new HashMap<IPluginExecutable, Long>());
    private ExecutorService threadPool = Executors.newCachedThreadPool();

    public ManagementAspect() {
        Timer pluginWatchdog = new Timer();
        pluginWatchdog.scheduleAtFixedRate(new PluginWatchdog(), CheckIntervalMillis, CheckIntervalMillis);
    }

    @Before("execution(void dst.ass3.aop.IPluginExecutable.execute())")
    public void interceptBefore(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Timeout timeout = signature.getMethod().getAnnotation(Timeout.class);

        if (timeout == null)
            return;

        IPluginExecutable plugin = (IPluginExecutable) joinPoint.getThis();
        pluginTimeouts.put(plugin, System.currentTimeMillis() + timeout.value());
    }

    @After("execution(void dst.ass3.aop.IPluginExecutable.execute())")
    public void interceptAfter(JoinPoint joinPoint) {
        IPluginExecutable plugin = (IPluginExecutable) joinPoint.getThis();
        pluginTimeouts.remove(plugin);
    }

    /**
     * Checks running plugin durations.
     */
    class PluginWatchdog extends TimerTask {

        @Override
        public void run() {
            synchronized (pluginTimeouts) {
                for (Map.Entry<IPluginExecutable, Long> pluginTimeout : pluginTimeouts.entrySet()) {
                    if (pluginTimeout.getValue() > System.currentTimeMillis()) {
                        interruptPlugin(pluginTimeout.getKey());
                        pluginTimeouts.remove(pluginTimeout.getKey());
                    }
                }
            }
        }

        private void interruptPlugin(final IPluginExecutable plugin) {
            System.out.println("PluginWatchdog: interrupting " + plugin.getClass().getName());
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    plugin.interrupted();
                }
            });
        }
    }

}
