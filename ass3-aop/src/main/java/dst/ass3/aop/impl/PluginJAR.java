package dst.ass3.aop.impl;

import dst.ass3.aop.IPluginExecutable;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

class PluginJAR {

    public PluginJAR(File file) {
        File = file;
        LastModified = file.lastModified();
        loadJAR();
    }

    private File File;
    private Long LastModified;
    private List<IPluginExecutable> Plugins = new ArrayList<IPluginExecutable>();

    public boolean IsDifferent(File file) {
        return file.lastModified() != LastModified;
    }

    /**
     * Loads a JAR, looks at all entries and instatiates all plugin classes.
     * https://stackoverflow.com/questions/11016092/how-to-load-classes-at-runtime-from-a-folder-or-jar.
     */
    private void loadJAR() {
        JarFile jarFile = null;
        Plugins.clear();

        try {
            jarFile = new JarFile(File);
            Enumeration entries = jarFile.entries();

            URL jarFileURL = File.toURI().toURL();
            ClassLoader jarClassCloader = new URLClassLoader(new URL[]{jarFileURL});

            while (entries.hasMoreElements()) {
                JarEntry entry = (JarEntry)entries.nextElement();
                if (entry.isDirectory() || !entry.getName().endsWith(".class"))
                    continue;

                String className = entry.getName().substring(0, entry.getName().length() - 6);
                className = className.replace("/", ".");

                Class clazz = jarClassCloader.loadClass(className);
                if (!IPluginExecutable.class.isAssignableFrom(clazz))
                    continue;

                System.out.println("Plugin: " + className);
                Plugins.add((IPluginExecutable)clazz.newInstance());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (jarFile != null)
                    jarFile.close();

            } catch (Exception ignored) {}
        }
    }

    /**
     * Runs the contained plugins.
     */
    public void executePlugins(ExecutorService threadPool) {
        for(final IPluginExecutable plugin : Plugins) {
            threadPool.execute(new Runnable() {
                @Override
                public void run() {
                    plugin.execute();
                }
            });
        }
    }
}
