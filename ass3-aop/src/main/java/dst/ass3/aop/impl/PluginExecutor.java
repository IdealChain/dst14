package dst.ass3.aop.impl;

import dst.ass3.aop.IPluginExecutor;

import java.io.File;

public class PluginExecutor implements IPluginExecutor {

    private PluginMonitorThread monitorThread = new PluginMonitorThread();

    @Override
    public void monitor(File dir) {
        System.out.println(String.format("PluginExecutor: monitor(%s)", dir));
        monitorThread.monitorDir(dir);
    }

    @Override
    public void stopMonitoring(File dir) {
        System.out.println(String.format("PluginExecutor: stopMonitoring(%s)", dir));
        monitorThread.unmonitorDir(dir);
    }

    @Override
    public void start() {
        System.out.println("PluginExecutor: start()");
        monitorThread.start();
    }

    @Override
    public void stop() {
        System.out.println("PluginExecutor: stop()");
        monitorThread.stopThread();
        System.out.println("PluginExecutor: stopped.");
    }
}

