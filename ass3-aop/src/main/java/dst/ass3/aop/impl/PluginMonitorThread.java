package dst.ass3.aop.impl;

import java.io.File;
import java.io.FileFilter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class PluginMonitorThread extends Thread {

    public static final int ScanIntervalMillis = 1000;

    private final Set<File> monitoredDirs = Collections.synchronizedSet(new HashSet<File>());
    private Map<File, PluginJAR> loadedJARS = new HashMap<File, PluginJAR>();
    private ExecutorService threadPool = Executors.newCachedThreadPool();

    public void monitorDir(File dir) {
        monitoredDirs.add(dir);
    }

    public void unmonitorDir(File dir) {
        monitoredDirs.remove(dir);
    }

    @Override
    public void run() {
        System.out.println("PluginMonitor: run()");
        try {
            while (!Thread.currentThread().isInterrupted()) {
                checkJARSUpdated();
                Thread.sleep(ScanIntervalMillis);
            }
        } catch (InterruptedException ignored) {
        }
        System.out.println("PluginMonitor: interrupted.");

        // clean up
        loadedJARS.clear();
    }

    public void stopThread() {
        try {
            this.interrupt();
            this.join();
        } catch (InterruptedException ignored) {
        }
    }

    private void checkJARSUpdated() {
        List<File> foundJARS = new ArrayList<File>();

        synchronized (monitoredDirs) {
            for (File dir : monitoredDirs) {
                foundJARS.addAll(Arrays.asList(dir.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File file) {
                        return file.isFile() && file.getName().endsWith(".jar") &&
                                (!loadedJARS.containsKey(file) || loadedJARS.get(file).IsDifferent(file));
                    }
                })));
            }
        }

        for (File jar : foundJARS)
            loadJAR(jar, loadedJARS.containsKey(jar));
    }

    private void loadJAR(File jar, boolean updated) {
        System.out.printf("Loading: %s (%s)%n", jar, updated ? "Updated" : "New");

        PluginJAR loaded = new PluginJAR(jar);
        loaded.executePlugins(threadPool);
        loadedJARS.put(jar, loaded);
    }
}
