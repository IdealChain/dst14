package dst.ass2.di.impl;

import dst.ass2.di.IInjectionController;
import dst.ass2.di.InjectionException;
import dst.ass2.di.annotation.Component;
import dst.ass2.di.annotation.Inject;
import dst.ass2.di.model.ScopeType;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import static org.springframework.util.ReflectionUtils.makeAccessible;
import static org.springframework.util.ReflectionUtils.setField;

public class InjectionController implements IInjectionController {

    private Map<Class<?>, Object> singletonInstances = new ConcurrentHashMap<Class<?>, Object>();
    private final ReentrantLock singletonConstructionLock = new ReentrantLock();

    private Long nextComponentId = 0L;
    private final Object nextComponentIdLock = new Object();

    protected boolean transparentMode;

    public InjectionController() {
        this(false);
    }

    public InjectionController(boolean transparentMode) {
        this.transparentMode = transparentMode;
    }

    @Override
    public void initialize(Object obj) throws InjectionException {
        Class<?> clazz = obj.getClass();
        Component compAnnotation = getComponentAnnotation(clazz);
        boolean singleton = compAnnotation.scope() == ScopeType.SINGLETON;
        Long componentId;

        System.out.println(String.format("Initialize @Component: %s", clazz.getName()));

        if (singleton)
            singletonConstructionLock.lock();

        try {
            if (singleton && singletonInstances.containsKey(clazz)) {
                throw new InjectionException(String.format(
                        "A %s singleton instance is already initialized", clazz.getName()));
            }

            // inject all fields from hierarchy
            for (Field field : FieldHelpers.getFieldHierarchy(clazz, new FieldHelpers.InjectFilter()))
                injectField(obj, field);

            // field injection successful: set component ID
            List<Field> componentIdFields = FieldHelpers.getFieldHierarchy(clazz, new FieldHelpers.ComponentIdFilter());
            if (componentIdFields.isEmpty())
                throw new InjectionException(String.format(
                        "%s does not have a valid @ComponentId field", clazz.getName()));

            synchronized (nextComponentIdLock) {
                componentId = nextComponentId++;
            }
            for (Field field : componentIdFields) {
                makeAccessible(field);
                setField(field, obj, componentId);
            }

            if (singleton)
                singletonInstances.put(clazz, obj);

        } finally {
            if (singleton)
                singletonConstructionLock.unlock();
        }

        System.out.println(String.format(
                "Initialize @Component: %s done (ID %d)", clazz.getName(), componentId));
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getSingletonInstance(Class<T> clazz) throws InjectionException {

        Component compAnnotation = getComponentAnnotation(clazz);
        if (compAnnotation.scope() != ScopeType.SINGLETON)
            throw new InjectionException(clazz.getName() + " @Component is not SINGLETON scoped");

        singletonConstructionLock.lock();
        try {
            if (!singletonInstances.containsKey(clazz)) {
                System.out.println("Creating singleton: " + clazz.getName());
                createInstance(clazz);
            } else {
                System.out.println("Returning existing singleton: " + clazz.getName());
            }
        } finally {
            singletonConstructionLock.unlock();
        }

        return (T) singletonInstances.get(clazz);
    }

    /**
     * Resolves and injects an instance for an {@link dst.ass2.di.annotation.Inject @Inject} annotated field.
     *
     * @param obj   the object to initialize.
     * @param field the {@link dst.ass2.di.annotation.Inject @Inject} annotated field.
     * @throws InjectionException if there is a type mismatch or a (nested) field object initialization fails.
     */
    private void injectField(Object obj, Field field) throws InjectionException {

        Inject injectAnnotation = field.getAnnotation(Inject.class);
        String fieldName = String.format("%s.%s", field.getDeclaringClass().getName(), field.getName());

        try {
            Class<?> injectedType = injectAnnotation.specificType();

            // specific type not supplied: fallback to field type
            if (injectedType == null || injectedType.equals(Void.class))
                injectedType = field.getType();

            if (!field.getType().isAssignableFrom(injectedType)) {
                throw new InjectionException(String.format(
                        "%s specific type is not assignable (%s -/-> %s)",
                        fieldName,
                        injectedType.getName(),
                        field.getType().getName()
                ));
            }

            System.out.println(String.format("Resolving @Inject: %s (%s)", fieldName, injectedType.getName()));

            makeAccessible(field);
            setField(field, obj, getInstance(injectedType));

        } catch (InjectionException e) {
            if (injectAnnotation.required())
                throw e;

            System.out.println("Unrequired @Inject not satisfied: " + fieldName);
            System.out.println(String.format("(%s)", e.getMessage()));
        }
    }

    private <T> T getInstance(Class<T> clazz) throws InjectionException {
        Component compAnnotation = getComponentAnnotation(clazz);

        if (compAnnotation.scope() == ScopeType.SINGLETON)
            return getSingletonInstance(clazz);
        else if (compAnnotation.scope() == ScopeType.PROTOTYPE)
            return createInstance(clazz);

        throw new InjectionException("Unexpected scope type: " + compAnnotation.scope());
    }

    private <T> T createInstance(Class<T> clazz) throws InjectionException {
        try {
            T instance = clazz.newInstance();

            if (!this.transparentMode)
                initialize(instance);

            return instance;

        } catch (InstantiationException e) {
            e.printStackTrace();
            throw new InjectionException(e);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            throw new InjectionException(e);
        }
    }

    private Component getComponentAnnotation(Class<?> clazz) throws InjectionException {
        Component compAnnotation = clazz.getAnnotation(Component.class);
        if (compAnnotation == null)
            throw new InjectionException(clazz.getName() + " is not annotated as @Component");
        return compAnnotation;
    }
}
