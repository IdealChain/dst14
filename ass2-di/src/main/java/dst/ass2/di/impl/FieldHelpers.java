package dst.ass2.di.impl;

import dst.ass2.di.annotation.ComponentId;
import dst.ass2.di.annotation.Inject;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class FieldHelpers {

    protected static List<Field> getFieldHierarchy(Class<?> clazz) {
        List<Field> fields = new ArrayList<Field>();
        for (Class<?> c = clazz; c != null; c = c.getSuperclass()) {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fields;
    }

    protected static List<Field> getFieldHierarchy(Class<?> clazz, ReflectionUtils.FieldFilter filter) {
        List<Field> fields = getFieldHierarchy(clazz);
        ListIterator<Field> iter = fields.listIterator();
        while (iter.hasNext()) {
            if (!filter.matches(iter.next()))
                iter.remove();
        }
        return fields;
    }

    protected static class ComponentIdFilter implements ReflectionUtils.FieldFilter {
        @Override
        public boolean matches(Field field) {
            return field.getType() == Long.class && field.isAnnotationPresent(ComponentId.class);
        }
    }

    protected static class InjectFilter implements ReflectionUtils.FieldFilter {
        @Override
        public boolean matches(Field field) {
            return field.isAnnotationPresent(Inject.class);
        }
    }
}
