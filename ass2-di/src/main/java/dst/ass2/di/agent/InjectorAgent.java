package dst.ass2.di.agent;

import dst.ass2.di.annotation.Component;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;

import java.io.ByteArrayInputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.security.ProtectionDomain;

public class InjectorAgent implements ClassFileTransformer {

    @Override
    public byte[] transform(ClassLoader loader, String className,
                            Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
                            byte[] classfileBuffer) throws IllegalClassFormatException {

        ClassPool pool = ClassPool.getDefault();
        CtClass componentClass = null;

        try {
            componentClass = pool.makeClass(new ByteArrayInputStream(classfileBuffer));

            if (!componentClass.hasAnnotation(Component.class))
                return classfileBuffer;

            for (CtConstructor con : componentClass.getConstructors()) {
                con.insertAfter("System.out.println(\"Injected component init: \" + this.getClass().getName());");
                con.insertAfter("dst.ass2.di.InjectionControllerFactory.getTransparentInstance().initialize(this);");
            }

            classfileBuffer = componentClass.toBytecode();

        } catch (Exception e) {
            System.out.println("ClassFileTransformer error: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (componentClass != null)
                componentClass.detach();
        }

        return classfileBuffer;
    }

    /**
     * Register our nice injector agent as class file transformer.
     * @param agentArgs arguments.
     * @param inst java instrumentation services.
     */
    public static void premain(String agentArgs, Instrumentation inst) {
        inst.addTransformer(new InjectorAgent());
    }

}
