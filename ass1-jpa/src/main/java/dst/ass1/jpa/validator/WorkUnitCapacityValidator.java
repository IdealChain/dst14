package dst.ass1.jpa.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WorkUnitCapacityValidator implements ConstraintValidator<WorkUnitCapacity, Integer> {

    private WorkUnitCapacity capacity;

    @Override
    public void initialize(WorkUnitCapacity workUnitCapacity) {
        this.capacity = workUnitCapacity;
    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {

        Boolean valid = integer >= capacity.min() && integer <= capacity.max();
        System.out.println(String.format(
                        "Validating WorkUnitCapacity %d [%d-%d]: %s",
                        integer,
                        capacity.min(),
                        capacity.max(),
                        valid ? "valid" : "invalid")
        );
        return valid;
    }
}
