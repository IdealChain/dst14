package dst.ass1.jpa.validator;

import javax.validation.*;
import java.lang.annotation.*;

@Constraint(validatedBy = WorkUnitCapacityValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface WorkUnitCapacity {
    String message() default "WorkUnitCapacity out of valid range";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    int min() default 0;
    int max() default Integer.MAX_VALUE;
}
