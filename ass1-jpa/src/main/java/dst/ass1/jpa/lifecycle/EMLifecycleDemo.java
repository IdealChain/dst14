package dst.ass1.jpa.lifecycle;

import java.security.NoSuchAlgorithmException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import dst.ass1.jpa.model.*;

public class EMLifecycleDemo {

    private EntityManager em;
    private ModelFactory modelFactory;

    public EMLifecycleDemo(EntityManager em, ModelFactory modelFactory) {
        this.em = em;
        this.modelFactory = modelFactory;
    }

    /**
     * Method to illustrate the persistence lifecycle. EntityManager is opened and
     * closed by the Test-Environment!
     *
     * @throws NoSuchAlgorithmException
     * @see <a href="http://www.objectdb.com/java/jpa/persistence/managed">Entity object lifecycle</a>
     */
    public void demonstrateEntityMangerLifecycle()
            throws NoSuchAlgorithmException {

        ModelFactory factory = new ModelFactory();
        EntityTransaction tx = null;

        try {
            tx = em.getTransaction();
            tx.begin();

            // create associated user entity
            IUser user = factory.createUser();
            user.setUsername("Reinhard");
            em.persist(user);

            // state: New (no persistent identity and not yet associated with a persistence context)
            ITask task = factory.createTask();

            // associate Task with User, TaskProcessing and Metadata
            task.setUser(user);
            task.setTaskProcessing(factory.createTaskProcessing());
            task.setMetadata(factory.createMetadata());

            // state: New -> Managed (persistent identity, associated with a persistence context)
            em.persist(task);
            tx.commit();

            // state: Managed -> Detached (persistent identity, but not currently associated with a persistence context)
            em.detach(task);

            // state: Detached -> Managed (persistent identity and associated with a persistence context)
            // returns the associated instance (previous task object stays detached)
            tx.begin();
            task = em.merge(task);
            tx.commit();

            // state: Managed -> Removed (persistent and associated, but scheduled for removal from the data store)
            tx.begin();
            em.remove(task);
            tx.commit();

        } catch (RuntimeException e) {
            if (tx != null && tx.isActive())
                tx.rollback();

            throw e;
        }
    }

}
