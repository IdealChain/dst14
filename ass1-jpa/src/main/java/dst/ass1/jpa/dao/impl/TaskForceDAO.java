package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ITaskForceDAO;
import dst.ass1.jpa.model.ITaskForce;
import dst.ass1.jpa.model.impl.TaskForce;
import org.hibernate.Session;

public class TaskForceDAO extends BaseDAO<ITaskForce> implements ITaskForceDAO {
    public TaskForceDAO(Session session) {
        super(session, TaskForce.class);
    }
}
