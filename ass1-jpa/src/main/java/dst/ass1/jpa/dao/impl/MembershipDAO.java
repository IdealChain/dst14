package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IMembershipDAO;
import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.impl.Membership;
import org.hibernate.Session;

public class MembershipDAO extends BaseDAO<IMembership> implements IMembershipDAO {
    public MembershipDAO(Session session) {
        super(session, Membership.class);
    }
}
