package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ITaskProcessingDAO;
import dst.ass1.jpa.model.ITaskProcessing;
import dst.ass1.jpa.model.impl.TaskProcessing;
import org.hibernate.Session;

public class TaskProcessingDAO extends BaseDAO<ITaskProcessing> implements ITaskProcessingDAO {
    public TaskProcessingDAO(Session session) {
        super(session, TaskProcessing.class);
    }
}
