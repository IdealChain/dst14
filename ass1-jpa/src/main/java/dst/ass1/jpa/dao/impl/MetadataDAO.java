package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IMetadataDAO;
import dst.ass1.jpa.model.IMetadata;
import dst.ass1.jpa.model.impl.Metadata;
import org.hibernate.Session;

public class MetadataDAO extends BaseDAO<IMetadata> implements IMetadataDAO {
    public MetadataDAO(Session session) {
        super(session, Metadata.class);
    }
}
