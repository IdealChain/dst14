package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.GenericDAO;
import org.hibernate.Session;

import java.util.List;

@SuppressWarnings("unchecked")
public abstract class BaseDAO<TInterface> implements GenericDAO<TInterface> {

    protected Session session;
    private Class type;

    public BaseDAO(Session session, Class concreteType) {
        this.session = session;
        this.type = concreteType;
    }

    public TInterface findById(Long id) {
        return (TInterface) session.get(type, id);
    }

    public List<TInterface> findAll() {
        return session.createCriteria(type).list();
    }
}
