package dst.ass1.jpa.dao;

import dst.ass1.jpa.dao.impl.*;

public class DAOFactory {

	private org.hibernate.Session session;

	public DAOFactory(org.hibernate.Session session) {
		this.session = session;
	}

	public IWorkPlatformDAO getPlatformDAO() {
		return new WorkPlatformDAO(session);
	}

	public IExpertDAO getExpertDAO() {
		return new ExpertDAO(session);
	}

	public ITaskForceDAO getTaskForceDAO() {
		return new TaskForceDAO(session);
	}

	public ITaskWorkerDAO getTaskWorkerDAO() {
		return new TaskWorkerDAO(session);
	}

	public IMetadataDAO getMetadataDAO() {
		return new MetadataDAO(session);
	}

	public ITaskProcessingDAO getTaskProcessingDAO() {
        return new TaskProcessingDAO(session);
	}

	public ITaskDAO getTaskDAO() {
        return new TaskDAO(session);
	}

	public IMembershipDAO getMembershipDAO() {
        return new MembershipDAO(session);
	}

	public IUserDAO getUserDAO() {
        return new UserDAO(session);
	}

}
