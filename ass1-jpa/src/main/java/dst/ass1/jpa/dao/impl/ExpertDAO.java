package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IExpertDAO;
import dst.ass1.jpa.model.IExpert;
import dst.ass1.jpa.model.impl.Expert;
import dst.ass1.jpa.util.Constants;
import org.hibernate.Session;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class ExpertDAO extends BaseDAO<IExpert> implements IExpertDAO {
    public ExpertDAO(Session session) {
        super(session, Expert.class);
    }

    @Override
    public HashMap<IExpert, Date> findNextTaskForceMeetingOfExperts() {

        HashMap<IExpert, Date> nextTaskForceMeetingOfExperts = new HashMap<IExpert, Date>();
        Iterator iter = session.
                getNamedQuery(Constants.Q_TASKFORCESOFEXPERT).
                setParameter("firstNamePattern", "Alex%").
                iterate();

        while (iter.hasNext()) {
            Object[] tfe = (Object[]) iter.next();
            nextTaskForceMeetingOfExperts.put((IExpert) tfe[0], (Date) tfe[1]);
        }

        return nextTaskForceMeetingOfExperts;
    }
}
