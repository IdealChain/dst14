package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IUserDAO;
import dst.ass1.jpa.model.IUser;
import dst.ass1.jpa.model.impl.User;
import org.hibernate.Session;

public class UserDAO extends BaseDAO<IUser> implements IUserDAO {
    public UserDAO(Session session) {
        super(session, User.class);
    }
}
