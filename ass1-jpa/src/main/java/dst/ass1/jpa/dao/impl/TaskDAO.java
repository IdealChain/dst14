package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ITaskDAO;
import dst.ass1.jpa.model.ITask;
import dst.ass1.jpa.model.TaskStatus;
import dst.ass1.jpa.model.impl.Task;
import dst.ass1.jpa.model.impl.TaskProcessing;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Restrictions;

import java.util.Date;
import java.util.List;

@SuppressWarnings("unchecked")
public class TaskDAO extends BaseDAO<ITask> implements ITaskDAO {
    public TaskDAO(Session session) {
        super(session, Task.class);
    }

    @Override
    public List<ITask> findTasksForUserAndContext(String user, String c) {
        Criteria tasks = session.createCriteria(Task.class);

        if (user != null)
            tasks.createCriteria("user").add(Restrictions.eq("username", user));

        if (c != null)
            tasks.createCriteria("metadata").add(Restrictions.eq("context", c));

        return tasks.list();
    }

    @Override
    public List<ITask> findTasksForStatusFinishedStartandFinish(Date start, Date finish) {
        Criteria tasks = session.createCriteria(Task.class);

        TaskProcessing example = new TaskProcessing();
        example.setStatus(TaskStatus.FINISHED);

        if (start != null)
            example.setStart(start);

        if (finish != null)
            example.setEnd(finish);

        tasks.createCriteria("taskProcessing").add(Example.create(example));
        return tasks.list();
    }
}
