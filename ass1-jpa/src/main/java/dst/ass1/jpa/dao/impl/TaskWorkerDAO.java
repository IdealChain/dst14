package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.ITaskWorkerDAO;
import dst.ass1.jpa.model.ITaskWorker;
import dst.ass1.jpa.model.impl.TaskWorker;
import org.hibernate.Session;

public class TaskWorkerDAO extends BaseDAO<ITaskWorker> implements ITaskWorkerDAO {
    public TaskWorkerDAO(Session session) {
        super(session, TaskWorker.class);
    }
}
