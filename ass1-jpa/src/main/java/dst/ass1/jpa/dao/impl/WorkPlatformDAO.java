package dst.ass1.jpa.dao.impl;

import dst.ass1.jpa.dao.IWorkPlatformDAO;
import dst.ass1.jpa.model.IWorkPlatform;
import dst.ass1.jpa.model.impl.WorkPlatform;
import org.hibernate.Session;

public class WorkPlatformDAO extends BaseDAO<IWorkPlatform> implements IWorkPlatformDAO {
    public WorkPlatformDAO(Session session) {
        super(session, WorkPlatform.class);
    }
}
