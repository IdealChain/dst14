package dst.ass1.jpa.interceptor;

import org.hibernate.EmptyInterceptor;

public class SQLInterceptor extends EmptyInterceptor {

    private static final long serialVersionUID = 3894614218727237142L;

    private static int selectCount = 0;
    private static final Object selectCountLock = new Object();

    public String onPrepareStatement(String sql) {

        String sqllc = sql.toLowerCase();
        if (sqllc.startsWith("select ") && (sqllc.contains("expert") || sqllc.contains("taskforce"))) {
            synchronized (selectCountLock) {
                selectCount++;
                System.out.println("Experts/TaskForces selects: " + selectCount);
            }
        }

        return sql;
    }

    public static void resetCounter() {
        synchronized (selectCountLock) {
            selectCount = 0;
        }
    }


    public static int getSelectCount() {
        synchronized (selectCountLock) {
            return selectCount;
        }
    }
}
