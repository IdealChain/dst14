package dst.ass1.jpa.listener;

import dst.ass1.jpa.model.impl.TaskWorker;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

@SuppressWarnings("UnusedDeclaration")
public class WorkerListener {

    @PrePersist
    public void setJoinedAndLastTraining(TaskWorker worker) {

        if (worker.getJoinedDate() == null)
            worker.setJoinedDate(now());

        if (worker.getLastTraining() == null)
            worker.setLastTraining(now());
    }

    @PreUpdate
    public void updateLastTraining(TaskWorker worker) {
        worker.setLastTraining(now());
    }

    private Date now() {
        return new Date();
    }

}
