package dst.ass1.jpa.listener;

import javax.persistence.*;

@SuppressWarnings("UnusedDeclaration")
public class DefaultListener {

    private static int loadOperations = 0;
    private static final Object loadOperationsLock = new Object();

    private static int updateOperations = 0;
    private static final Object updateOperationsLock = new Object();

    private static int removeOperations = 0;
    private static final Object removeOperationsLock = new Object();

    private static int persistOperations = 0;
    private static long overallPersistTime = 0;
    private static ThreadLocal<Long> localPersistStart = new ThreadLocal<Long>();
    private static final Object persistOperationsLock = new Object();

    public static int getLoadOperations() {
        synchronized (loadOperationsLock) {
            return loadOperations;
        }
    }

    public static int getUpdateOperations() {
        synchronized (updateOperationsLock) {
            return updateOperations;
        }
    }

    public static int getRemoveOperations() {
        synchronized (removeOperationsLock) {
            return removeOperations;
        }
    }

    public static int getPersistOperations() {
        synchronized (persistOperationsLock) {
            return persistOperations;
        }
    }

    public static long getOverallTimeToPersist() {
        synchronized (persistOperationsLock) {
            return overallPersistTime;
        }
    }

    public static double getAverageTimeToPersist() {
        synchronized (persistOperationsLock) {
            return 1.0 * overallPersistTime / persistOperations;
        }
    }

    public static void clear() {
        System.out.println("Clearing default listener's stats...");
        synchronized (loadOperationsLock) {
            loadOperations = 0;
        }
        synchronized (updateOperationsLock) {
            updateOperations = 0;
        }
        synchronized (removeOperationsLock) {
            removeOperations = 0;
        }
        synchronized (persistOperationsLock) {
            persistOperations = 0;
            overallPersistTime = 0;
            localPersistStart.remove();
        }
    }

    @PrePersist
    public static void onPrePersist(Object o)
    {
        localPersistStart.set(System.nanoTime());
    }

    @PostPersist
    public static void onPostPersist(Object o)
    {
        synchronized (persistOperationsLock) {
            persistOperations++;
            overallPersistTime += (System.nanoTime() - localPersistStart.get());

            System.out.println(String.format(
                    "Persist operations: %d (%.3f ms overall)",
                    persistOperations,
                    (double)overallPersistTime / 1000000));
        }
    }

    @PostRemove
    public static void onRemove(Object o)
    {
        synchronized (removeOperationsLock) {
            removeOperations++;
            System.out.println("Remove operations: " + removeOperations);
        }
    }

    @PostUpdate
    public static void onUpdate(Object o)
    {
        synchronized (updateOperationsLock) {
            updateOperations++;
            System.out.println("Update operations: " + updateOperations);
        }
    }

    @PostLoad
    public static void onLoad(Object o)
    {
        synchronized (loadOperationsLock) {
            loadOperations++;
            System.out.println("Load operations: " + loadOperations);
        }
    }

}
