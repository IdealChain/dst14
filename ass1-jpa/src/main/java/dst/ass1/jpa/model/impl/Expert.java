package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IExpert;
import dst.ass1.jpa.model.ITaskForce;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Expert extends Person implements IExpert {

    @OneToMany(targetEntity = TaskForce.class, mappedBy = "expert")
    private List<ITaskForce> taskForces;

    @Override
    public List<ITaskForce> getAdvisedTaskForces() {
        return taskForces;
    }

    @Override
    public void setAdvisedTaskForces(List<ITaskForce> taskForces) {
        this.taskForces = taskForces;
    }

    @Override
    public void addAdvisedTaskForce(ITaskForce taskForce) {
        if (this.taskForces == null)
            this.taskForces = new ArrayList<ITaskForce>();

        this.taskForces.add(taskForce);
    }
}
