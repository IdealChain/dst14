package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.ITask;
import dst.ass1.jpa.model.ITaskProcessing;
import dst.ass1.jpa.model.ITaskWorker;
import dst.ass1.jpa.model.TaskStatus;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "TaskProcessing.runningTasks",
                query = "SELECT tp " +
                        "FROM TaskProcessing tp " +
                        "WHERE tp.start < :now AND tp.end IS NULL"
        ),
        @NamedQuery(
                name = "TaskProcessing.finishedForPlatform",
                query = "SELECT tp FROM TaskProcessing tp " +
                        "JOIN tp.taskWorkers tw " +
                        "JOIN tw.taskForce tf " +
                        "JOIN tf.workPlatform wp " +
                        "WHERE tp.status = 'FINISHED' AND wp = :platform " +
                        "ORDER BY tp.end DESC " +
                        "LIMIT :limit"
        )
})

public class TaskProcessing implements ITaskProcessing {

    @Id
    @GeneratedValue
    private long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    private Date start;

    @Override
    public Date getStart() {
        return start;
    }

    @Override
    public void setStart(Date start) {
        this.start = start;
    }

    private Date end;

    @Override
    public Date getEnd() {
        return end;
    }

    @Override
    public void setEnd(Date end) {
        this.end = end;
    }

    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @Override
    public TaskStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    @ManyToMany(targetEntity = TaskWorker.class)
    @JoinTable(name = Constants.J_PROCESSING_TASKWORKER)
    private List<ITaskWorker> taskWorkers;

    @Override
    public List<ITaskWorker> getTaskWorkers() {
        return taskWorkers;
    }

    @Override
    public void setTaskWorkers(List<ITaskWorker> list) {
        taskWorkers = list;
    }

    @Override
    public void addWorker(ITaskWorker worker) {
        if (taskWorkers == null)
            taskWorkers = new ArrayList<ITaskWorker>();

        taskWorkers.add(worker);
    }

    @OneToOne(targetEntity = Task.class, mappedBy = "taskProcessing", optional = false)
    private ITask task;

    @Override
    public ITask getTask() {
        return task;
    }

    @Override
    public void setTask(ITask task) {
        this.task = task;
    }
}
