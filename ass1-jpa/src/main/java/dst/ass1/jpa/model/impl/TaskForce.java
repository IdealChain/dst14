package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IExpert;
import dst.ass1.jpa.model.ITaskForce;
import dst.ass1.jpa.model.ITaskWorker;
import dst.ass1.jpa.model.IWorkPlatform;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NamedQuery(
        name = Constants.Q_TASKFORCESOFEXPERT,
        query = "SELECT tf.expert, MIN(tf.nextMeeting) " +
                "FROM TaskForce tf " +
                "WHERE tf.expert.firstName LIKE :firstNamePattern " +
                "GROUP BY tf.expert"
)
public class TaskForce implements ITaskForce {

    @Id
    @GeneratedValue
    private long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Column(unique = true)
    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    private Date lastMeeting;

    @Override
    public Date getLastMeeting() {
        return lastMeeting;
    }

    @Override
    public void setLastMeeting(Date lastMeeting) {
        this.lastMeeting = lastMeeting;
    }

    private Date nextMeeting;

    @Override
    public Date getNextMeeting() {
        return nextMeeting;
    }

    @Override
    public void setNextMeeting(Date nextMeeting) {
        this.nextMeeting = nextMeeting;
    }

    @ManyToMany(targetEntity = TaskForce.class, mappedBy = "partOf")
    private List<ITaskForce> composedOf;

    @Override
    public List<ITaskForce> getComposedOf() {
        return composedOf;
    }

    @Override
    public void setComposedOf(List<ITaskForce> composedOf) {
        this.composedOf = composedOf;
    }

    @Override
    public void addComposedOf(ITaskForce o) {
        if (composedOf == null)
            composedOf = new ArrayList<ITaskForce>();

        composedOf.add(o);
    }

    @ManyToMany(targetEntity = TaskForce.class)
    @JoinTable(name = "composed_of")
    private List<ITaskForce> partOf;

    @Override
    public List<ITaskForce> getPartOf() {
        return partOf;
    }

    @Override
    public void setPartOf(List<ITaskForce> partOf) {
        this.partOf = partOf;
    }

    @Override
    public void addPartOf(ITaskForce o) {
        if (partOf == null)
            partOf = new ArrayList<ITaskForce>();

        partOf.add(o);
    }

    @OneToMany(targetEntity = TaskWorker.class, mappedBy = "taskForce", cascade = CascadeType.ALL)
    private List<ITaskWorker> taskWorkers;

    @Override
    public List<ITaskWorker> getTaskWorkers() {
        return taskWorkers;
    }

    @Override
    public void setTaskWorkers(List<ITaskWorker> worker) {
        taskWorkers = worker;
    }

    @Override
    public void addTaskWorker(ITaskWorker worker) {
        if (taskWorkers == null)
            taskWorkers = new ArrayList<ITaskWorker>();

        taskWorkers.add(worker);
    }

    @ManyToOne(targetEntity = Expert.class, optional = false)
    private IExpert expert;

    @Override
    public IExpert getExpert() {
        return expert;
    }

    @Override
    public void setExpert(IExpert expert) {
        this.expert = expert;
    }

    @ManyToOne(targetEntity = WorkPlatform.class, optional = false)
    private IWorkPlatform workPlatform;

    @Override
    public IWorkPlatform getWorkPlatform() {
        return workPlatform;
    }

    @Override
    public void setWorkPlatform(IWorkPlatform platform) {
        this.workPlatform = platform;
    }
}
