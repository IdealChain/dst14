package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.ITask;
import dst.ass1.jpa.model.IUser;
import dst.ass1.jpa.util.Constants;
import org.hibernate.annotations.Index;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"accountNo", "bankCode"}))
@NamedQueries({
        @NamedQuery(
                name = Constants.Q_USERSWITHACTIVEMEMBERSHIP,
                query = "SELECT m.id.user " +
                        "FROM Membership m " +
                        "WHERE m.id.workPlatform.name = :name AND " +
                        "(SELECT COUNT(t)" +
                        " FROM Task t " +
                        " JOIN t.taskProcessing tp " +
                        " JOIN tp.taskWorkers tws " +
                        " JOIN tws.taskForce tf " +
                        " JOIN tf.workPlatform wp " +
                        " WHERE t.user = m.id.user AND wp.name = :name) >= :minNr"
        ),
        @NamedQuery(
                name = Constants.Q_MOSTACTIVEUSER,
                query = "SELECT u " +
                        "FROM User u " +
                        "WHERE u.tasks.size > 0 " +
                        "AND u.tasks.size = (SELECT MAX(u.tasks.size) FROM User u)"
        ),
        @NamedQuery(
                name = "User.byName",
                query = "SELECT u FROM User u " +
                        "WHERE u.username = :name"
        ),
        @NamedQuery(
                name = "User.verifyUser",
                query = "SELECT u FROM User u " +
                        "WHERE u.username = :name AND u.password = :pw"
        )
})
public class User extends Person implements IUser {

    @Column(unique = true, nullable = false)
    private String username;

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Column(length = 16)
    @Index(name = "PASSWORD_IDX")
    private byte[] password;

    @Override
    public byte[] getPassword() {
        if (password == null)
            return new byte[0];

        return password;
    }

    @Override
    public void setPassword(byte[] password) {
        this.password = password;
    }

    private String accountNo;

    @Override
    public String getAccountNo() {
        return accountNo;
    }

    @Override
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    private String bankCode;

    @Override
    public String getBankCode() {
        return bankCode;
    }

    @Override
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @OneToMany(targetEntity = Task.class, mappedBy = "user", cascade = CascadeType.ALL)
    private List<ITask> tasks;

    @Override
    public List<ITask> getTasks() {
        return tasks;
    }

    @Override
    public void setTasks(List<ITask> tasks) {
        this.tasks = tasks;
    }

    @Override
    public void addTask(ITask task) {
        if (tasks == null)
            tasks = new ArrayList<ITask>();

        tasks.add(task);
    }

    @OneToMany(targetEntity = Membership.class)
    private List<IMembership> memberships;

    @Override
    public void addMembership(IMembership membership) {
        if (memberships == null)
            memberships = new ArrayList<IMembership>();

        memberships.add(membership);
    }

    @Override
    public List<IMembership> getMemberships() {
        return memberships;
    }

    @Override
    public void setMemberships(List<IMembership> memberships) {
        this.memberships = memberships;
    }
}
