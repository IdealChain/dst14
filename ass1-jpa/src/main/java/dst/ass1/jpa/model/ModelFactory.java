package dst.ass1.jpa.model;

import dst.ass1.jpa.model.impl.*;

import java.io.Serializable;

public class ModelFactory implements Serializable {

	private static final long serialVersionUID = 1L;

	public IAddress createAddress() {
		return new Address();
	}

	public IExpert createExpert() {
		return new Expert();
	}

	public ITaskForce createTaskForce() {
		return new TaskForce();
	}

	public ITaskWorker createTaskWorker() {
		return new TaskWorker();
	}

	public IMetadata createMetadata() {
		return new Metadata();
	}

	public ITaskProcessing createTaskProcessing() {
		return new TaskProcessing();
	}

	public IWorkPlatform createPlatform() {
		return new WorkPlatform();
	}

	public ITask createTask() {
		return new Task();
	}

	public IMembership createMembership() {
		return new Membership();
	}

	public IMembershipKey createMembershipKey() {
		return new MembershipKey();
	}

	public IUser createUser() {
		return new User();
	}

}
