package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.ITaskForce;
import dst.ass1.jpa.model.IWorkPlatform;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQuery(
        name = "WorkPlatform.byName",
        query = "SELECT wp FROM WorkPlatform wp " +
                "WHERE wp.name = :name"
)
public class WorkPlatform implements IWorkPlatform {

    @Id
    @GeneratedValue
    private long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Column(unique = true)
    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    private String location;

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public void setLocation(String location) {
        this.location = location;
    }

    private BigDecimal costsPerWorkUnit;

    @Override
    public BigDecimal getCostsPerWorkUnit() {
        return costsPerWorkUnit;
    }

    @Override
    public void setCostsPerWorkUnit(BigDecimal costsPerWorkUnit) {
        this.costsPerWorkUnit = costsPerWorkUnit;
    }

    @ElementCollection(targetClass = Membership.class)
    private List<IMembership> memberships;

    @Override
    public void addMembership(IMembership membership) {
        memberships.add(membership);
    }

    @Override
    public List<IMembership> getMemberships() {
        return memberships;
    }

    @Override
    public void setMemberships(List<IMembership> memberships) {
        this.memberships = memberships;
    }

    @OneToMany(targetEntity = TaskForce.class, mappedBy = "workPlatform", cascade = CascadeType.ALL)
    private List<ITaskForce> taskForces;

    @Override
    public List<ITaskForce> getTaskForces() {
        return taskForces;
    }

    @Override
    public void setTaskForces(List<ITaskForce> taskForces) {
        this.taskForces = taskForces;
    }

    @Override
    public void addTaskForce(ITaskForce taskForce) {
        if (taskForces == null)
            taskForces = new ArrayList<ITaskForce>();

        taskForces.add(taskForce);
    }
}
