package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMembership;
import dst.ass1.jpa.model.IMembershipKey;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQuery(
        name = "Membership.forUserAndPlatform",
        query = "SELECT m FROM Membership m " +
                "WHERE m.id.user = :user AND m.id.workPlatform = :platform"
)
public class Membership implements IMembership {

    @EmbeddedId
    private MembershipKey id;

    @Override
    public IMembershipKey getId() {
        return id;
    }

    @Override
    public void setId(IMembershipKey id) {
        if (!(id instanceof MembershipKey))
            throw new IllegalArgumentException("MemberShipKey type required");

        this.id = (MembershipKey)id;
    }

    private Date registration;

    @Override
    public Date getRegistration() {
        return registration;
    }

    @Override
    public void setRegistration(Date registration) {
        this.registration = registration;
    }

    private Double discount;

    @Override
    public Double getDiscount() {
        return discount;
    }

    @Override
    public void setDiscount(Double discount) {
        this.discount = discount;
    }
}
