package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMembershipKey;
import dst.ass1.jpa.model.IUser;
import dst.ass1.jpa.model.IWorkPlatform;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class MembershipKey implements IMembershipKey, Serializable {

    @ManyToOne(targetEntity = User.class, optional = false)
    private IUser user;

    @ManyToOne(targetEntity = WorkPlatform.class, optional = false)
    private IWorkPlatform workPlatform;

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public void setUser(IUser user) {
        this.user = user;
    }

    @Override
    public IWorkPlatform getWorkPlatform() {
        return workPlatform;
    }

    @Override
    public void setWorkPlatform(IWorkPlatform platform) {
        workPlatform = platform;
    }

    public int hashCode() {
        return user.hashCode() + workPlatform.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null) return false;
        if (!(obj instanceof MembershipKey)) return false;
        MembershipKey key = (MembershipKey) obj;
        return key.user == user && key.workPlatform == workPlatform;
    }
}
