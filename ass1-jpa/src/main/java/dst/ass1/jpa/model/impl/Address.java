package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IAddress;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class Address implements IAddress, Serializable {

    private String street;
    private String city;
    private String zipCode;

    @Override
    public String getStreet() {
        return street;
    }

    @Override
    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String getZipCode() {
        return zipCode;
    }

    @Override
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public int hashCode() {
        return street.hashCode() + city.hashCode() + zipCode.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null) return false;
        if (!(obj instanceof Address)) return false;
        Address adr = (Address) obj;
        return adr.street.equals(street) && adr.city.equals(city) && adr.zipCode.equals(zipCode);
    }
}
