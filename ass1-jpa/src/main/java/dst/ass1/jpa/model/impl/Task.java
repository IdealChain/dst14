package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.*;
import dst.ass1.jpa.util.Constants;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(
                name = Constants.Q_ALLFINISHEDTASKS,
                query = "SELECT t FROM Task t " +
                        "WHERE t.taskProcessing.status = 'FINISHED'"
        ),
        @NamedQuery(
                name = "Task.numPaidForUser",
                query = "SELECT COUNT(t) FROM Task t " +
                        "WHERE t.user = :user " +
                        "AND t.isPaid = true"
        ),
        @NamedQuery(
                name = "Task.unpaidForUser",
                query = "SELECT t FROM Task t " +
                        "JOIN t.taskProcessing tp " +
                        "WHERE t.user = :user " +
                        "AND t.isPaid != true " +
                        "AND tp.status = 'FINISHED'"
        )
})

public class Task implements ITask {

    @Id
    @GeneratedValue
    private long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Integer getAssignedWorkUnits() {

        Integer workUnits = 0;

        ITaskProcessing tp = getTaskProcessing();
        if (tp != null) {
            for (ITaskWorker worker : tp.getTaskWorkers())
                workUnits += worker.getWorkUnitCapacity();
        }

        return workUnits;
    }

    @Override
    public void setAssignedWorkUnits(Integer workUnits) {
    }

    @Override
    public Integer getProcessingTime() {

        ITaskProcessing tp = getTaskProcessing();
        if (tp != null) {
            Date start = tp.getStart();
            Date end = tp.getEnd();

            if (start != null && end != null) {
                Long diff = (end.getTime() - start.getTime()) / 1000;
                return diff.intValue();
            }
        }

        return 0;
    }

    @Override
    public void setProcessingTime(Integer processingTime) {
    }

    private boolean isPaid;

    @Override
    public boolean isPaid() {
        return isPaid;
    }

    @Override
    public void setPaid(boolean isPaid) {
        this.isPaid = isPaid;
    }

    @OneToOne(targetEntity = Metadata.class, cascade = CascadeType.ALL, optional = false)
    @JoinColumn(unique = true)
    private IMetadata metadata;

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(IMetadata metadata) {
        this.metadata = metadata;
    }

    @ManyToOne(targetEntity = User.class, optional = false)
    private IUser user;

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public void setUser(IUser user) {
        this.user = user;
    }

    @OneToOne(targetEntity = TaskProcessing.class, cascade = CascadeType.ALL, optional = false)
    private ITaskProcessing taskProcessing;

    @Override
    public ITaskProcessing getTaskProcessing() {
        return taskProcessing;
    }

    @Override
    public void setTaskProcessing(ITaskProcessing processing) {
        this.taskProcessing = processing;
    }
}
