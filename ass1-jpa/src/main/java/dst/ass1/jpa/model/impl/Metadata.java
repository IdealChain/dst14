package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.model.IMetadata;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Metadata implements IMetadata {

    @Id
    @GeneratedValue
    private long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    private String context;

    @Override
    public String getContext() {
        return context;
    }

    @Override
    public void setContext(String description) {
        context = description;
    }

    @ElementCollection
    @OrderColumn
    private List<String> settings;

    @Override
    public List<String> getSettings() {
        return settings;
    }

    @Override
    public void setSettings(List<String> settings) {
        this.settings = settings;
    }

    @Override
    public void addSetting(String setting) {
        if (settings == null)
            settings = new ArrayList<String>();

        settings.add(setting);
    }
}
