package dst.ass1.jpa.model.impl;

import dst.ass1.jpa.listener.WorkerListener;
import dst.ass1.jpa.model.*;
import dst.ass1.jpa.validator.WorkUnitCapacity;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.*;

@Entity
@EntityListeners(WorkerListener.class)
@NamedQueries({
        @NamedQuery(
                name = "TaskWorker.free",
                query = "SELECT tw FROM TaskWorker tw " +
                        "WHERE tw NOT IN " +
                        "(SELECT tw " +
                        " FROM TaskWorker tw " +
                        " JOIN tw.taskProcessings tp " +
                        " WHERE tp.status = 'PROCESSING' OR tp.status = 'SCHEDULED')"
        ),
        @NamedQuery(
                name = "TaskWorker.freeOnPlatform",
                query = "SELECT tw FROM TaskWorker tw " +
                        "JOIN tw.taskForce tf " +
                        "JOIN tf.workPlatform wp " +
                        "WHERE wp.id = :platformId AND tw NOT IN " +
                        "(SELECT tw " +
                        " FROM TaskWorker tw " +
                        " JOIN tw.taskProcessings tp " +
                        " WHERE tp.status = 'PROCESSING' OR tp.status = 'SCHEDULED')"
        )
})
public class TaskWorker implements ITaskWorker {

    @Id
    @GeneratedValue
    private long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Size(min = 5, max = 25)
    @Column(unique = true)
    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @WorkUnitCapacity(min = 4, max = 8)
    private Integer workUnitCapacity;

    @Override
    public Integer getWorkUnitCapacity() {
        return workUnitCapacity;
    }

    @Override
    public void setWorkUnitCapacity(Integer workUnits) {
        this.workUnitCapacity = workUnits;
    }

    @Pattern(regexp = "^[A-Z]{3}-[A-Z]{3}@[0-9]{4,}$")
    private String location;

    @Override
    public String getLocation() {
        return location;
    }

    @Override
    public void setLocation(String location) {
        this.location = location;
    }

    @Past
    private Date joinedDate;

    @Override
    public Date getJoinedDate() {
        return joinedDate;
    }

    @Override
    public void setJoinedDate(Date joined) {
        this.joinedDate = joined;
    }

    @Past
    private Date lastTraining;

    @Override
    public Date getLastTraining() {
        return lastTraining;
    }

    @Override
    public void setLastTraining(Date lastTraining) {
        this.lastTraining = lastTraining;
    }

    @ManyToOne(targetEntity = TaskForce.class, optional = false)
    private ITaskForce taskForce;

    @Override
    public ITaskForce getTaskForce() {
        return taskForce;
    }

    @Override
    public void setTaskForce(ITaskForce taskForce) {
        this.taskForce = taskForce;
    }

    @ManyToMany(targetEntity = TaskProcessing.class, mappedBy = "taskWorkers")
    private List<ITaskProcessing> taskProcessings;

    @Override
    public List<ITaskProcessing> getTaskProcessings() {
        return taskProcessings;
    }

    @Override
    public void setTaskProcessings(List<ITaskProcessing> processings) {
        this.taskProcessings = processings;
    }

    @Override
    public void addTaskProcessing(ITaskProcessing processing) {
        if (taskProcessings == null)
            taskProcessings = new ArrayList<ITaskProcessing>();

        taskProcessings.add(processing);
    }
}
