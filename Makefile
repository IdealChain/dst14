LVA=dst14
MATR=0926807
TAGS=$(shell git tag -l)
ZIPS=$(TAGS:%=$(LVA)_%_$(MATR).zip)

.PHONY: zip clean $(TAGS)

zip: $(ZIPS)

$(ZIPS): $(LVA)_%_$(MATR).zip: %
	git archive  --format zip -o $@ refs/tags/$<
	touch -d "$(shell git show -s --pretty="format:%cD" refs/tags/$< | tail -1)" $@

$(TAGS):

clean:
	mvn clean -Pall
